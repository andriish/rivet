BEGIN PLOT /TASSO_1986_I228876/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-\pi^0$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
