BEGIN PLOT /CELLO_1989_I266414/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^\pm\pi^\mp$ 
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to K^0_SK^\pm\pi^\mp)$ [nb]
LogY=1
END PLOT
