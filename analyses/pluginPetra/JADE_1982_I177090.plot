BEGIN PLOT /JADE_1982_I177090/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta^\prime$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
