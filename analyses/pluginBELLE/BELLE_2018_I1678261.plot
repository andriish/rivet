BEGIN PLOT /BELLE_2018_I1678261/d01-x01-y01
Title=Cross section for $e^+e^-\to \pi^+\pi^-\pi^0 \chi_{b(1,2)}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0 \chi_{b(1,2)})$ [pb]
LogY=0
ConnectGaps=1
END PLOT
