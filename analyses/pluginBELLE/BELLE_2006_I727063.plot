BEGIN PLOT /BELLE_2006_I727063/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \Sigma^0\bar\Sigma^0$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \Sigma^0\bar\Sigma^0)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2006_I727063/d01-x01-y02
Title=Cross section for $\gamma\gamma\to \Lambda^0\bar\Lambda^0$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \Lambda^0\bar\Lambda^0)$ [nb]
LogY=1
END PLOT
