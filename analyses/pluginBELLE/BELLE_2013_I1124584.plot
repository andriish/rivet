BEGIN PLOT /BELLE_2013_I1124584/d01-x01-y01
Title=Helicity angle $\cos\theta_1$ in $B^0_s\to D^{*+}_sD^{*-}_s$
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_1$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1124584/d01-x01-y02
Title=Helicity angle $\cos\theta_2$ in $B^0_s\to D^{*+}_sD^{*-}_s$
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_2$
LogY=0
END PLOT
