BEGIN PLOT /BELLE_2006_I689881/d01-x01-y01
Title=Kaon helicity angle for $B^0\to \chi_{c1} K^{*0}$
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT
