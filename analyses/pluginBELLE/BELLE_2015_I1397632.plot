# BEGIN PLOT /BELLE_2015_I1397632/d01-x01-y01
Title=$B^0\to D^-e^+\nu$
XLabel=$\omega$
YLabel=${\rm d}\Gamma/{\rm d}\omega$ [$10^{-15}\times\text{GeV}$]
# END PLOT

# BEGIN PLOT /BELLE_2015_I1397632/d01-x01-y02
Title=$B^0\to D^-\mu^+\nu$
XLabel=$\omega$
YLabel=${\rm d}\Gamma/{\rm d}\omega$ [$10^{-15}\times\text{GeV}$]
# END PLOT

# BEGIN PLOT /BELLE_2015_I1397632/d02-x01-y01
Title=$B^+\to D^0e^+\nu$
XLabel=$\omega$
YLabel=${\rm d}\Gamma/{\rm d}\omega$ [$10^{-15}\times\text{GeV}$]
# END PLOT

# BEGIN PLOT /BELLE_2015_I1397632/d02-x01-y02
Title=$B^+\to D^0\mu^+\nu$
XLabel=$\omega$
YLabel=${\rm d}\Gamma/{\rm d}\omega$ [$10^{-15}\times\text{GeV}$]
# END PLOT
