BEGIN PLOT /BELLE_2021_I1748231
LogY=0
XLabel=$q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /BELLE_2021_I1748231/d01
YLabel=$\text{d}\mathcal{B}/\text{d}q^2\times 10^7$ [$\text{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BELLE_2021_I1748231/d01-x01
Title=Differential branching ratio w.r.t $q^2$ for $B^+\to K^+\mu^+\mu^-$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d01-x02
Title=Differential branching ratio w.r.t $q^2$ for $B^0\to K^0_S\mu^+\mu^-$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d01-x03
Title=Differential branching ratio w.r.t $q^2$ for $B^+\to K^+e^+e^-$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d01-x04
Title=Differential branching ratio w.r.t $q^2$ for $B^0\to K^0_Se^+e^-$
END PLOT

BEGIN PLOT /BELLE_2021_I1748231/d02-x01
Title=Asymmetry $A_I$ for muon modes
YLabel=$A_I$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d02-x02
Title=Asymmetry $A_I$ for electron modes
YLabel=$A_I$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d02-x03
Title=Asymmetry $A_I$ for electron+muon modes
YLabel=$A_I$
END PLOT

BEGIN PLOT /BELLE_2021_I1748231/d03-x01
Title=Ratio of $R_K$ for $B^+$ decays
YLabel=$R_{K^+}$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d03-x02
Title=Ratio of $R_K$ for $B^0$ decays
YLabel=$R_{K^0}$
END PLOT
BEGIN PLOT /BELLE_2021_I1748231/d03-x03
Title=Ratio of $R_K$ for sum of $B^0$ and $B^+$ decays
YLabel=$R_K$
END PLOT
