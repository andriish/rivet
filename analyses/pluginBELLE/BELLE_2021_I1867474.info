Name: BELLE_2021_I1867474
Year: 2021
Summary: Mass distributions in $D^0\to\pi^+\pi^-\eta$ and $D^0\to K^+K^-\eta$
Experiment: BELLE
Collider: KEKB
InspireID: 1867474
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 09 (2021) 075
RunInfo: Any process producing D0 mesons
Description:
  'Measurement of the mass distributions in the decays $D^0\to\pi^+\pi^-\eta$ and $D^0\to K^+K^-\eta$ by BELLE. The data were read from the plots in the paper, and for many points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2021dfa
BibTeX: '@article{Belle:2021dfa,
    author = "Li, L. K. and others",
    collaboration = "Belle",
    title = "{Measurement of branching fractions and search for $CP$ violation in $D^{0}\to\pi^{+}\pi^{-}\eta$, $D^{0}\to K^{+}K^{-}\eta$, and $D^{0}\to\phi\eta$ at Belle}",
    eprint = "2106.04286",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "KEK Preprint 2021-8, Belle Preprint 2021-11, UCHEP-21-03",
    doi = "10.1007/JHEP09(2021)075",
    journal = "JHEP",
    volume = "09",
    pages = "075",
    year = "2021"
}
'
