BEGIN PLOT /BELLE_2006_I700451/d01-x01-y01
Title=$D^0\bar{D}^0$ mass in $\gamma\gamma\to D^0\bar{D}^0$
XLabel=$m_{D^0\bar{D}^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0\bar{D}^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I700451/d01-x01-y02
Title=$D^+D^-$ mass in $\gamma\gamma\to D^+D^-$
XLabel=$m_{D^+D^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^+D^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I700451/d01-x01-y03
Title=$D\bar{D}$ mass in $\gamma\gamma\to D\bar{D}$
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2006_I700451/d02-x01-y01
Title=$D\bar{D}$ mass in $\gamma\gamma\to D\bar{D}$ ($|\cos\theta^*|<0.5$)
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I700451/d02-x01-y02
Title=$D\bar{D}$ mass in $\gamma\gamma\to D\bar{D}$ ($|\cos\theta^*|>0.5$)
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2006_I700451/d03-x01-y01
Title=$D$ $\cos\theta$ distribution for $3.91<m_{D\bar{D}}<3.95\,$GeV
XLabel=$|\cos\theta|$
YLabel=$1/\sigma\text{d}\sigma/\text{d}|\cos\theta|$
LogY=0
END PLOT
