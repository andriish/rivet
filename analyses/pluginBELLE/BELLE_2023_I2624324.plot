BEGIN PLOT /BELLE_2023_I2624324/d0[1,2]-x01
LogY=0
XLabel=$w$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}w$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d0[1,2]-x02
LogY=0
XLabel=$\cos\theta_\ell$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\ell$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d0[1,2]-x03
LogY=0
XLabel=$\cos\theta_v$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_v$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d0[1,2]-x04
LogY=0
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$ [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x01-y01
Title=$w$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x01-y02
Title=$w$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x01-y03
Title=$w$ distribution for $B^+\to D^{*0} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x01-y04
Title=$w$ distribution for $B^+\to D^{*0} \mu^+\nu_\mu$
END PLOT

BEGIN PLOT /BELLE_2023_I2624324/d01-x02-y01
Title=$\cos\theta_\ell$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x02-y02
Title=$\cos\theta_\ell$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x02-y03
Title=$\cos\theta_\ell$ distribution for $B^+\to D^{*0} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x02-y04
Title=$\cos\theta_\ell$ distribution for $B^+\to D^{*0} \mu^+\nu_\mu$
END PLOT

BEGIN PLOT /BELLE_2023_I2624324/d01-x03-y01
Title=$\cos\theta_v$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x03-y02
Title=$\cos\theta_v$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x03-y03
Title=$\cos\theta_V$ distribution for $B^+\to D^{*0} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x03-y04
Title=$\cos\theta_V$ distribution for $B^+\to D^{*0} \mu^+\nu_\mu$
END PLOT

BEGIN PLOT /BELLE_2023_I2624324/d01-x04-y01
Title=$\chi$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x04-y02
Title=$\chi$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x04-y03
Title=$\chi$ distribution for $B^+\to D^{*0} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d01-x04-y04
Title=$\chi$ distribution for $B^+\to D^{*0} \mu^+\nu_\mu$
END PLOT

BEGIN PLOT /BELLE_2023_I2624324/d02-x01-y01
Title=$w$ distribution for $B\to D^* \ell^+\nu_\ell$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d02-x02-y01
Title=$\cos\theta_\ell$ distribution for $B\to D^* \ell^+\nu_\ell$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d02-x03-y01
Title=$\cos\theta_v$ distribution for $B\to D^* \ell^+\nu_\ell$
END PLOT
BEGIN PLOT /BELLE_2023_I2624324/d02-x04-y01
Title=$\chi$ distribution for $B\to D^* \ell^+\nu_\ell$
END PLOT
