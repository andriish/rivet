BEGIN PLOT /BELLE_2005_I677873/d01-x01-y01
Title=Kaon helicity angle for $B^0\to \phi K^{*0}$
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I677873/d01-x02-y01
Title=Kaon helicity angle for $B^+\to \phi K^{*+}$
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I677873/d01-x01-y02
Title=$\cos\theta_{\text{tr}}$ for $B^0\to \phi K^{*0}$
XLabel=$\cos\theta_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\text{tr}}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I677873/d01-x02-y02
Title=$\cos\theta_{\text{tr}}$ for $B^+\to \phi K^{*+}$
XLabel=$\cos\theta_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\text{tr}}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I677873/d01-x01-y03
Title=$\phi_{\text{tr}}$ for $B^0\to \phi K^{*0}$
XLabel=$\phi_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_{\text{tr}}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I677873/d01-x02-y03
Title=$\phi_{\text{tr}}$ for $B^+\to \phi K^{*+}$
XLabel=$\phi_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_{\text{tr}}$
LogY=0
END PLOT
