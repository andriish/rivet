BEGIN PLOT /BELLE_2007_I732595/d01-x01-y01
Title=$\langle m_X^2\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$E^*_{\ell,\min}$ [GeV]
YLabel=$\langle m_X^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2007_I732595/d01-x01-y02
Title=$\langle \left(m_X^2-\langle m_X^2\rangle\right)^2\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$E^*_{\ell,\min}$ [GeV]
YLabel=$\langle \left(m_X^2-\langle m_X^2\rangle^2\right)^2\rangle$ [$\text{GeV}^4$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2007_I732595/d01-x01-y03
Title=$\langle m_X^4\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$E^*_{\ell,\min}$ [GeV]
YLabel=$\langle m_X^4\rangle$ [$\text{GeV}^4$]
LogY=0
END PLOT
