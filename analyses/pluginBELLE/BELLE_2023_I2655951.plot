BEGIN PLOT /BELLE_2023_I2655951/d01-x01-y01
Title=$K^-K^0_S$ mass in $B^-\to D^0 K^-K^0_S$
XLabel=$m_{K^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2655951/d01-x01-y02
Title=$K^-K^0_S$ mass in $\bar{B}^0\to D^+ K^-K^0_S$
XLabel=$m_{K^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2655951/d01-x01-y03
Title=$K^-K^0_S$ mass in $B^-\to D^{*0} K^-K^0_S$
XLabel=$m_{K^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2655951/d01-x01-y04
Title=$K^-K^0_S$ mass in $\bar{B}^0\to D^{*+} K^-K^0_S$
XLabel=$m_{K^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
