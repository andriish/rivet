Name: BELLE_2005_I665011
Year: 2005
Summary: Electron spectrum in semileptonic $B$ meson decays
Experiment: BELLE
Collider: KEKB
InspireID: 665011
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - 
RunInfo: Any process producing B0 and B+ mesons, originally Upsilon(4S) decays
Description:
  'Measurement of the electron spectrum in semileptonic $B$ meson decays, the data are fully corrected and were taken from Table 2 in the paper.'
ValidationInfo:
  'Herwig7 events using EvtGen for B decays.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2004ibv
BibTeX: '@article{Belle:2004ibv,
    author = "Okabe, T. and others",
    collaboration = "Belle",
    title = "{Spectra of prompt electrons from decays of B+ and B0 mesons and ratio of inclusive semielectronic branching fractions}",
    eprint = "hep-ex/0411066",
    archivePrefix = "arXiv",
    reportNumber = "BELLE-2004-32, KEK-PREPRINT-2004-65",
    doi = "10.1016/j.physletb.2005.03.060",
    journal = "Phys. Lett. B",
    volume = "614",
    pages = "27--36",
    year = "2005"
}
'
