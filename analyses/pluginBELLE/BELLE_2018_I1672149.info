Name: BELLE_2018_I1672149
Year: 2018
Summary: $\gamma\gamma\to\eta^\prime\pi^+\pi^-$ for centre-of-mass energies between 1.4 and 3.8 GeV
Experiment: BELLE
Collider: KEKB
InspireID: 1672149
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 98 (2018) 7, 072001
RunInfo: gamma gamma to hadrons
Beams: [gamma, gamma]
Energies: []
Description:
'Measurement of the cross section for $\gamma\gamma\to\eta^\prime\pi^+\pi^-$ for centre-of-mass energies between 1.4 and 3.8 GeV. The resonance subprocess
$\gamma\gamma\to f_2(1270)\eta^\prime$ is alos measured, and subtracted from the final result above 2.26 GeV. The contribution from $\eta_c(1)$ is also subtracted between
2.62,3.06 GeV.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2018bry
BibTeX: '@article{Belle:2018bry,
    author = "Xu, Q. N. and others",
    collaboration = "Belle",
    title = "{Measurement of $\eta_c(1S), \eta_c(2S)$ and non-resonant $\eta^\prime \pi^+ pi^-$ production via two-photon collisions}",
    eprint = "1805.03044",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.98.072001",
    journal = "Phys. Rev. D",
    volume = "98",
    number = "7",
    pages = "072001",
    year = "2018"
}
'
