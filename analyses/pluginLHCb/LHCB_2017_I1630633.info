Name: LHCB_2017_I1630633
Year: 2017
Summary: $B^\pm$ meson production at 7 and 13 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1630633
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 12 (2017) 026
RunInfo: hadronic events with B+ mesons
Beams: [p+, p+]
Energies: [7000,13000]
Options:
 - ENERGY=7000,13000
Description:
'Measurement of the double differential (in $p_\perp$ and $y$) cross section for $B^\pm$ meson production at 7 and 13 TeV by the LHCb collaboration.
 Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2017vec
BibTeX: '@article{LHCb:2017vec,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Measurement of the $B^{\pm}$ production cross-section in pp collisions at $\sqrt{s} =$ 7 and 13 TeV}",
    eprint = "1710.04921",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-EP-2017-254, LHCB-PAPER-2017-037",
    doi = "10.1007/JHEP12(2017)026",
    journal = "JHEP",
    volume = "12",
    pages = "026",
    year = "2017"
}'
