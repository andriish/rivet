BEGIN PLOT /LHCB_2017_I1630633/d0[1,2]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2017_I1630633/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d01-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 7 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2017_I1630633/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 13 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 13 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 13 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 13 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ at 13 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2017_I1630633/d03
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d03-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $B^+$ at 7 TeV 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d03-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $B^+$ at 13 TeV
END PLOT

BEGIN PLOT /LHCB_2017_I1630633/d04
XLabel=$y$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ $\mu$
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d04-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $B^+$ at 7 TeV 
END PLOT
BEGIN PLOT /LHCB_2017_I1630633/d04-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $B^+$ at 13 TeV
END PLOT
