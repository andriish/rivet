BEGIN PLOT /LHCB_2014_I1315113/d01-x01-y01
LOGY=0
Title=Ratio of $\chi_{b2}(1P)$ to $\chi_{b1}(1P)$ vs $p_\perp^\Upsilon$ using decay to $\Upsilon\gamma$
XLabel=$p_\perp^\Upsilon$ [GeV]
YLabel=$\sigma(\chi_{b2}(1P))/\sigma(\chi_{b1}(1P))$
LogY=0
END PLOT
