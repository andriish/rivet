BEGIN PLOT /LHCB_2023_I2643022/d01-x01-y01
Title=$K^+\eta^\prime$ mass in $B^+\to K^+\eta^\prime J/\psi$
XLabel=$m_{K^+\eta^\prime}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\eta^\prime}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2023_I2643022/d01-x01-y02
Title=$J/\psi\eta^\prime$ mass in $B^+\to K^+\eta^\prime J/\psi$
XLabel=$m_{J/\psi\eta^\prime}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\eta^\prime}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2023_I2643022/d01-x01-y03
Title=$K^+J/\psi$ mass in $B^+\to K^+\eta^\prime J/\psi$
XLabel=$m_{K^+J/\psi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+J/\psi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
