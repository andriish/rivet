Name: LHCB_2023_I2633007
Year: 2023
Summary: Differential branching ratio w.r.t. $q^2$ for $\Lambda^b_0\to\Lambda(1520)^0\mu^+\mu^-$
Experiment: LHCB
Collider: LHC
InspireID: 2633007
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2302.08262 [hep-ex]
RunInfo: Any process producing Lambda_b0 baryons, originally pp
Description:
'Measurement of the differential branching ratio w.r.t. $q^2$ for $\Lambda^b_0\to\Lambda(1520)^0\mu^+\mu^-$.
 The data were taken from the table in the paper.'
ValidationInfo:
  'Herwig7 events using EvtGen for B meson decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2023ptw
BibTeX: '@article{LHCb:2023ptw,
    collaboration = "LHCb",
    title = "{Measurement of the $\Lambda_{b}^{0}\to \Lambda(1520) \mu^{+}\mu^{-}$ differential branching fraction}",
    eprint = "2302.08262",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCb-PAPER-2022-050, CERN-EP-2023-007",
    month = "2",
    year = "2023"
}
'
