BEGIN PLOT /LHCB_2013_I1244315/d0[1,2]
YMin=-1.
YMax=1.
LogY=0
XLabel=$p_\perp$ [GeV]
END PLOT

BEGIN PLOT /LHCB_2013_I1244315/d0[1,2]-x01
YLabel=$\lambda_\theta$
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d0[1,2]-x02
YLabel=$\lambda_{\theta\phi}$
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d0[1,2]-x03
YLabel=$\lambda_\phi$
END PLOT


BEGIN PLOT /LHCB_2013_I1244315/d01-x01-y01
Title=$\lambda_\theta$ in helicity frame for J/$\psi$ ($2.0<y<2.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x01-y02
Title=$\lambda_\theta$ in helicity frame for J/$\psi$ ($2.5<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x01-y03
Title=$\lambda_\theta$ in helicity frame for J/$\psi$ ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x01-y04
Title=$\lambda_\theta$ in helicity frame for J/$\psi$ ($3.5<y<4.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x01-y05
Title=$\lambda_\theta$ in helicity frame for J/$\psi$ ($4.0<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2013_I1244315/d01-x02-y01
Title=$\lambda_{\theta\phi}$ in helicity frame for J/$\psi$ ($2.0<y<2.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x02-y02
Title=$\lambda_{\theta\phi}$ in helicity frame for J/$\psi$ ($2.5<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x02-y03
Title=$\lambda_{\theta\phi}$ in helicity frame for J/$\psi$ ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x02-y04
Title=$\lambda_{\theta\phi}$ in helicity frame for J/$\psi$ ($3.5<y<4.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x02-y05
Title=$\lambda_{\theta\phi}$ in helicity frame for J/$\psi$ ($4.0<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2013_I1244315/d01-x03-y01
Title=$\lambda_\phi$ in helicity frame for J/$\psi$ ($2.0<y<2.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x03-y02
Title=$\lambda_\phi$ in helicity frame for J/$\psi$ ($2.5<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x03-y03
Title=$\lambda_\phi$ in helicity frame for J/$\psi$ ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x03-y04
Title=$\lambda_\phi$ in helicity frame for J/$\psi$ ($3.5<y<4.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d01-x03-y05
Title=$\lambda_\phi$ in helicity frame for J/$\psi$ ($4.0<y<4.5$)
END PLOT


BEGIN PLOT /LHCB_2013_I1244315/d02-x01-y01
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($2.0<y<2.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x01-y02
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($2.5<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x01-y03
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x01-y04
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($3.5<y<4.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x01-y05
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($4.0<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2013_I1244315/d02-x02-y01
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($2.0<y<2.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x02-y02
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($2.5<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x02-y03
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x02-y04
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($3.5<y<4.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x02-y05
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($4.0<y<4.5$)
END PLOT

BEGIN PLOT /LHCB_2013_I1244315/d02-x03-y01
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($2.0<y<2.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x03-y02
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($2.5<y<3.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x03-y03
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($3.0<y<3.5$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x03-y04
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($3.5<y<4.0$)
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d02-x03-y05
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($4.0<y<4.5$)
END PLOT


BEGIN PLOT /LHCB_2013_I1244315/d04
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
LogY=1
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for J/$\psi$ ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for J/$\psi$ ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d04-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for J/$\psi$ ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d04-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for J/$\psi$ ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1244315/d04-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for J/$\psi$ ($4.0<y<4.5$) 
END PLOT
