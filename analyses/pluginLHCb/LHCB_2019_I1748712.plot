BEGIN PLOT /LHCB_2019_I1748712/d0[1,2,3,4]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d09
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d10
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d0[5,7]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d0[6,8]
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d11
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma(\psi(2S))/\sigma(J/\psi)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d12
XLabel=$y$
YLabel=$\sigma(\psi(2S))/\sigma(J/\psi)$
LogY=0
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d01-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d03-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d03-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d03-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d04-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d04-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d04-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d05-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d06-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d06-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d07-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d07-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d08-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d08-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=7$\,TeV
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d09-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$, $\sqrt{s}=13$\,TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d09-x01-y02
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=13$\,TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d09-x01-y03
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=13$\,TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d09-x01-y04
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=13$\,TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d09-x01-y05
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=13$\,TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2019_I1748712/d10-x01-y01
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=7$\,TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d10-x01-y02
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=7$\,TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d10-x01-y03
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=7$\,TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d10-x01-y04
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=7$\,TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d10-x01-y05
Title=Non-prompt $\psi(2S)$ fraction, $\sqrt{s}=7$\,TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d11-x01-y01
Title=Prompt $\psi(2S)$ to $J/\psi$ ratio at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d11-x01-y02
Title=Non-Prompt $\psi(2S)$ to $J/\psi$ ratio at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d12-x01-y01
Title=Prompt $\psi(2S)$ to $J/\psi$ ratio at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1748712/d12-x01-y02
Title=Non-Prompt $\psi(2S)$ to $J/\psi$ ratio at 13 TeV
END PLOT