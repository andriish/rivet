BEGIN PLOT /LHCB_2014_I1309880/d01-x01-y01
Title=$p\bar{p}$ mass distribution in $B_c^+\to J/\psi p\bar{p}\pi^+$
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2014_I1309880/d01-x01-y02
Title=$p\pi^+$ mass distribution in $B_c^+\to J/\psi p\bar{p}\pi^+$
XLabel=$m_{p\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
