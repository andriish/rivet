BEGIN PLOT /LHCB_2019_I1760257
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d01-x01-y01
YLabel=$f_s/f_d$
XLabel=$\sqrt{s}$ [TeV]
ConnectGaps=1
Title=$B^0_s/B^+$ ratio vs centre-of-mass energy
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d02-x01-y01
YLabel=$f_s/f_d/(f_s/f_d)_{7 \text{TeV}}$
XLabel=$\sqrt{s}$ [TeV]
ConnectGaps=1
Title=$B^0_s/B^+$ ratio w.r.t. value at 7 TeV vs centre-of-mass energy
ConnectGaps=1
END PLOT

BEGIN PLOT /LHCB_2019_I1760257/d03-x01-y01
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$ $0<p_L<75$\,GeV
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d03-x01-y02
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$ $75<p_L<125$\,GeV
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d03-x01-y03
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$ $125<p_L<700$\,GeV
END PLOT

BEGIN PLOT /LHCB_2019_I1760257/d03-x01-y01
YLabel=$f_s/f_d$ 
XLabel=$|p|$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $|p|$
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d04-x01-y02
YLabel=$f_s/f_d$ 
XLabel=$p_L$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_L$
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d04-x01-y03
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d04-x01-y04
YLabel=$f_s/f_d$ 
XLabel=$\eta$
Title=$B^0_s/B^+$ ratio w.r.t. $\eta$
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d04-x01-y05
YLabel=$f_s/f_d$ 
XLabel=$y$
Title=$B^0_s/B^+$ ratio w.r.t. $y$
END PLOT

BEGIN PLOT /LHCB_2019_I1760257/d05-x01-y01
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d05-x01-y02
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2019_I1760257/d05-x01-y03
YLabel=$f_s/f_d$ 
XLabel=$p_\perp$ [GeV]
Title=$B^0_s/B^+$ ratio w.r.t. $p_\perp$ at 13 TeV
END PLOT
