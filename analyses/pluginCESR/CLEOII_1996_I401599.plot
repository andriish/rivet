BEGIN PLOT /CLEOII_1996_I401599/d02-x01-y01
Title=Scaled $D_s^+$ momentum spectrum
XLabel=$x_p$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_p$
LogY=0
END PLOT
