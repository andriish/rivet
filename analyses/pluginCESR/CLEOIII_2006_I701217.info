Name: CLEOIII_2006_I701217
Year: 2006
Summary: Photon Spectrum in $\Upsilon(1,2,3S)$ decays
Experiment: CLEOIII
Collider: CESR
InspireID: 701217
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 74 (2006) 012003
RunInfo: Any process producing Upsilon mesons, original e+e- collisions
Description:
  'Measurement of the photon spectrum in $\Upsilon(1,2,3S)$ decays by CLEO.
   The spectrum was read from the figures in the paper and is not corrected
   for efficiency/resolution. The energy smearing given in the paper is applied
   to the photon energies'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2005mdr
BibTeX: '@article{CLEO:2005mdr,
    author = "Besson, D. and others",
    collaboration = "CLEO",
    title = "{Measurement of the direct photon momentum spectrum in upsilon(1S), upsilon(2S), and upsilon(3S) decays}",
    eprint = "hep-ex/0512061",
    archivePrefix = "arXiv",
    reportNumber = "CLNS-05-1927, CLEO-05-15",
    doi = "10.1103/PhysRevD.74.012003",
    journal = "Phys. Rev. D",
    volume = "74",
    pages = "012003",
    year = "2006"
}
'
