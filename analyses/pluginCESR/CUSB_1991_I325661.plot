BEGIN PLOT /CUSB_1991_I325661/d01-x01-y01
Title=Cross Section for $B^*$ production
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma_B$ [nb]
LogY=0
ConnectGaps=1
END PLOT
