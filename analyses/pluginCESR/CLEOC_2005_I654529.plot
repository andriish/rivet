BEGIN PLOT /CLEOC_2005_I654529/d01-x01
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT

BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y01
Title=Cross section for $e^+e^-\to\pi^+\pi^-\pi^0$
YLabel=$\sigma(e^+e^-\to\pi^+\pi^-\pi^0)$ [pb]
END PLOT

BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y02
Title=Cross section for $e^+e^-\to\rho\pi$
YLabel=$\sigma(e^+e^-\to\rho\pi)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y03
Title=Cross section for $e^+e^-\to\rho^0\pi^0$
YLabel=$\sigma(e^+e^-\to\rho^0\pi^0)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y04
Title=Cross section for $e^+e^-\to\rho^\pm\pi^\mp$
YLabel=$\sigma(e^+e^-\to\rho^\pm\pi^\mp)$ [pb]
END PLOT

BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y05
Title=Cross section for $e^+e^-\to\omega\pi^0$
YLabel=$\sigma(e^+e^-\to\omega\pi^0)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y06
Title=Cross section for $e^+e^-\to\phi\pi^0$
YLabel=$\sigma(e^+e^-\to\phi\pi^0)$ [pb]
END PLOT

BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y07
Title=Cross section for $e^+e^-\to\rho^0\eta$
YLabel=$\sigma(e^+e^-\to\rho^0\eta)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y08
Title=Cross section for $e^+e^-\to\omega\eta$
YLabel=$\sigma(e^+e^-\to\omega\eta)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y09
Title=Cross section for $e^+e^-\to\phi\eta$
YLabel=$\sigma(e^+e^-\to\phi\eta)$ [pb]
END PLOT

BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y10
Title=Cross section for $e^+e^-\to K^{*0}\bar{K}^0$+c.c
YLabel=$\sigma(e^+e^-\to K^{*0}\bar{K}^0+\text{c.c})$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y11
Title=Cross section for $e^+e^-\to K^{*\pm}K^\mp$
YLabel=$\sigma(e^+e^-\to K^{*\pm}K^\mp)$ [pb]
END PLOT

BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y12
Title=Cross section for $e^+e^-\to b_1\pi$
YLabel=$\sigma(e^+e^-\to b_1\pi)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y13
Title=Cross section for $e^+e^-\to b_1^0\pi^0$
YLabel=$\sigma(e^+e^-\to b_1^0\pi^0)$ [pb]
END PLOT
BEGIN PLOT /CLEOC_2005_I654529/d01-x01-y14
Title=Cross section for $e^+e^-\to b_1^\pm\pi^\mp$
YLabel=$\sigma(e^+e^-\to b_1^\pm\pi^\mp)$ [pb]
END PLOT
