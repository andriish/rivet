BEGIN PLOT /CLEO_1988_I23509/d0[2,3]-x01-y01
Title=$\sigma(e^+e^-\to c\bar{c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /CLEO_1988_I23509/d0[2,3]-x01-y02
Title=$R=\sigma(e^+e^-\to c\bar{c})/\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$R$
LogY=0
ConnectGaps=1
END PLOT
