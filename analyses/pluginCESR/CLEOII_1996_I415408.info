Name: CLEOII_1996_I415408
Year: 1996
Summary: Mass Distributions in $\tau$ decays with $K^0_S$ mesons
Experiment: CLEOII
Collider: CESR
InspireID: 415408
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 53 (1996) 6037-6053
RunInfo: Any process producing tau leptons, originally e+e-
Description:
'Mass distributions in the decays $\tau\to\nu_\tau+K^0_S\pi^-$, $K^-K^0_S$, $K^0_S\pi^-\pi^0$, $K^0_SK^-\pi^0$ and
 $K^0_SK^0_S\pi^-$. The background subtracted data were read from the figures in the paper.'
ValidationInfo:
  'Herwig7 event'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:1996rit
BibTeX: '@article{CLEO:1996rit,
    author = "Coan, T. E. and others",
    collaboration = "CLEO",
    title = "{Decays of tau leptons to final states containing K(s)0 mesons}",
    reportNumber = "CLNS-96-1391, CLEO-96-3, SLAC-PUB-9943",
    doi = "10.1103/PhysRevD.53.6037",
    journal = "Phys. Rev. D",
    volume = "53",
    pages = "6037--6053",
    year = "1996"
}
'
