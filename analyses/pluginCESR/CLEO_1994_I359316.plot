BEGIN PLOT /CLEO_1994_I359316/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\chi_{c2}$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
