BEGIN PLOT /CLEOII_2003_I616827/
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2\times10^4$ [$\text{GeV}^{-2}$]
END PLOT

BEGIN PLOT /CLEOII_2003_I616827/d01-x01-y01
Title=$B^0 \to \pi^- \ell^+ \nu_\ell$
LogY=0
END PLOT

BEGIN PLOT /CLEOII_2003_I616827/d02-x01-y01
Title=$B^0 \to \rho^- \ell^+ \nu_\ell$
LogY=0
END PLOT
