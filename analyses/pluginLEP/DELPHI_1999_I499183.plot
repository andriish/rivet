BEGIN PLOT /DELPHI_1999_I499183/d01-x01-y01
Title=Average $1-T$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle 1-T \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d01-x01-y02
Title=Average $(1-T)^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle(1-T)^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d01-x01-y03
Title=Average $(1-T)^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle(1-T)^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d02-x01-y01
Title=Average $T_\mathrm{major}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle T_\mathrm{major} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d02-x01-y02
Title=Average $T_\mathrm{major}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle T_\mathrm{major}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d02-x01-y03
Title=Average $T_\mathrm{major}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle T_\mathrm{major}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d03-x01-y01
Title=Average $T_\mathrm{minor}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle T_\mathrm{minor} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d023-x01-y02
Title=Average $T_\mathrm{minor}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle T_\mathrm{minor}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d03-x01-y03
Title=Average $T_\mathrm{minor}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle T_\mathrm{minor}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d04-x01-y01
Title=Average $O$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle O \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d04-x01-y02
Title=Average $O^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle O^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d04-x01-y03
Title=Average $O^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle O^3\rangle$
LogY=0
END PLOT


BEGIN PLOT /DELPHI_1999_I499183/d05-x01-y01
Title=Average $\rho_\mathrm{heavy}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{heavy} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d05-x01-y02
Title=Average $\rho_\mathrm{heavy}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{heavy}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d05-x01-y03
Title=Average $\rho_\mathrm{heavy}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{heavy}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d06-x01-y01
Title=Average $\rho_\mathrm{light}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{light} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d06-x01-y02
Title=Average $\rho_\mathrm{light}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{light}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d06-x01-y03
Title=Average $\rho_\mathrm{light}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{light}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d07-x01-y01
Title=Average $\rho_\mathrm{diff}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{diff} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d07-x01-y02
Title=Average $\rho_\mathrm{diff}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{diff}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d07-x01-y03
Title=Average $\rho_\mathrm{diff}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle \rho_\mathrm{diff}^3\rangle$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d08-x01-y01
Title=Average $B_\mathrm{max}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{max} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d08-x01-y02
Title=Average $B_\mathrm{max}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{max}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d08-x01-y03
Title=Average $B_\mathrm{max}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{max}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d09-x01-y01
Title=Average $B_\mathrm{min}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{min} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d09-x01-y02
Title=Average $B_\mathrm{min}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{min}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d09-x01-y03
Title=Average $B_\mathrm{min}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{min}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d10-x01-y01
Title=Average $B_\mathrm{sum}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{sum} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d10-x01-y02
Title=Average $B_\mathrm{sum}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{sum}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d10-x01-y03
Title=Average $B_\mathrm{sum}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{sum}^3\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d11-x01-y01
Title=Average $B_\mathrm{diff}$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{diff} \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d11-x01-y02
Title=Average $B_\mathrm{diff}^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{diff}^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d11-x01-y03
Title=Average $B_\mathrm{diff}^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle B_\mathrm{diff}^3\rangle$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d12-x01-y01
Title=Average $C$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle C \rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d12-x01-y02
Title=Average $C^2$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle C^2\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d12-x01-y03
Title=Average $C^3$ vs $\sqrt{s}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle C^3\rangle$
LogY=0
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d13-x01-y01
Title=Thrust ($E_\mathrm{CMS}=133$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d13-x01-y02
Title=Thrust ($E_\mathrm{CMS}=161$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d13-x01-y03
Title=Thrust ($E_\mathrm{CMS}=172$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d14-x01-y01
Title=Thrust ($E_\mathrm{CMS}=183$ GeV)
XLabel=$1-T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}(1-T)$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d15-x01-y01
Title=Major ($E_\mathrm{CMS}=133$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d15-x01-y02
Title=Major ($E_\mathrm{CMS}=161$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d15-x01-y03
Title=Major ($E_\mathrm{CMS}=172$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d16-x01-y01
Title=Major ($E_\mathrm{CMS}=183$ GeV)
XLabel=$T_\mathrm{major}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{major}$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d17-x01-y01
Title=Minor ($E_\mathrm{CMS}=133$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d17-x01-y02
Title=Minor ($E_\mathrm{CMS}=161$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d17-x01-y03
Title=Minor ($E_\mathrm{CMS}=172$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d18-x01-y01
Title=Minor ($E_\mathrm{CMS}=183$ GeV)
XLabel=$T_\mathrm{minor}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}T_\mathrm{minor}$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d19-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=133$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d19-x01-y02
Title=Oblateness ($E_\mathrm{CMS}=161$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d19-x01-y03
Title=Oblateness ($E_\mathrm{CMS}=172$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d20-x01-y01
Title=Oblateness ($E_\mathrm{CMS}=183$ GeV)
XLabel=$O$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}O$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d21-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=133$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d21-x01-y02
Title=Sphericity ($E_\mathrm{CMS}=161$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d21-x01-y03
Title=Sphericity ($E_\mathrm{CMS}=172$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d22-x01-y01
Title=Sphericity ($E_\mathrm{CMS}=183$ GeV)
XLabel=$S$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}S$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d23-x01-y01
Title=Planarity ($E_\mathrm{CMS}=133$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d23-x01-y02
Title=Planarity ($E_\mathrm{CMS}=161$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d23-x01-y03
Title=Planarity ($E_\mathrm{CMS}=172$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d24-x01-y01
Title=Planarity ($E_\mathrm{CMS}=183$ GeV)
XLabel=$P$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}P$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d25-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=133$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d25-x01-y02
Title=Aplanarity ($E_\mathrm{CMS}=161$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d25-x01-y03
Title=Aplanarity ($E_\mathrm{CMS}=172$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d26-x01-y01
Title=Aplanarity ($E_\mathrm{CMS}=183$ GeV)
XLabel=$A$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}A$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d27-x01-y01
Title=Heavy Jet Mass ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d27-x01-y02
Title=Heavy Jet Mass ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d27-x01-y03
Title=Heavy Jet Mass ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d28-x01-y01
Title=Heavy Jet Mass ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d29-x01-y01
Title=Light Jet Mass ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d29-x01-y02
Title=Light Jet Mass ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d29-x01-y03
Title=Light Jet Mass ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d30-x01-y01
Title=Light Jet Mass ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d31-x01-y01
Title=Jet Mass difference ($E_\mathrm{CMS}=133$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d31-x01-y02
Title=Jet Mass difference ($E_\mathrm{CMS}=161$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d31-x01-y03
Title=Jet Mass difference ($E_\mathrm{CMS}=172$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d32-x01-y01
Title=Jet Mass difference ($E_\mathrm{CMS}=183$ GeV)
XLabel=$\rho$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}\rho$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d33-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=133$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d33-x01-y02
Title=Wide jet broadening ($E_\mathrm{CMS}=161$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d33-x01-y03
Title=Wide jet broadening ($E_\mathrm{CMS}=172$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d34-x01-y01
Title=Wide jet broadening ($E_\mathrm{CMS}=183$ GeV)
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d35-x01-y01
Title=Narrow jet broadening ($E_\mathrm{CMS}=133$ GeV)
XLabel=$B_N$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_N$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d35-x01-y02
Title=Narrow jet broadening ($E_\mathrm{CMS}=161$ GeV)
XLabel=$B_N$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_N$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d35-x01-y03
Title=Narrow jet broadening ($E_\mathrm{CMS}=172$ GeV)
XLabel=$B_N$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_N$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d36-x01-y01
Title=Narrow jet broadening ($E_\mathrm{CMS}=183$ GeV)
XLabel=$B_N$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_N$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d37-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=133$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d37-x01-y02
Title=Total jet broadening ($E_\mathrm{CMS}=161$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d37-x01-y03
Title=Total jet broadening ($E_\mathrm{CMS}=172$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d38-x01-y01
Title=Total jet broadening ($E_\mathrm{CMS}=183$ GeV)
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d39-x01-y01
Title=Difference of the jet broadenings ($E_\mathrm{CMS}=133$ GeV)
XLabel=$B_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_D$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d39-x01-y02
Title=Difference of the jet broadenings ($E_\mathrm{CMS}=161$ GeV)
XLabel=$B_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_D$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d39-x01-y03
Title=Difference of the jet broadenings ($E_\mathrm{CMS}=172$ GeV)
XLabel=$B_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_D$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d40-x01-y01
Title=Difference of the jet broadenings ($E_\mathrm{CMS}=183$ GeV)
XLabel=$B_D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_D$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d41-x01-y01
Title=C-parameter ($E_\mathrm{CMS}=133$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d41-x01-y02
Title=C-parameter ($E_\mathrm{CMS}=161$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d41-x01-y03
Title=C-parameter ($E_\mathrm{CMS}=172$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d42-x01-y01
Title=C-parameter ($E_\mathrm{CMS}=183$ GeV)
XLabel=$C$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}C$
END PLOT

BEGIN PLOT /DELPHI_1999_I499183/d43-x01-y01
Title=D-parameter ($E_\mathrm{CMS}=133$ GeV)
XLabel=$D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}D$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d43-x01-y02
Title=D-parameter ($E_\mathrm{CMS}=161$ GeV)
XLabel=$D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}D$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d43-x01-y03
Title=D-parameter ($E_\mathrm{CMS}=172$ GeV)
XLabel=$D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}D$
END PLOT
BEGIN PLOT /DELPHI_1999_I499183/d44-x01-y01
Title=D-parameter ($E_\mathrm{CMS}=183$ GeV)
XLabel=$D$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}D$
END PLOT
