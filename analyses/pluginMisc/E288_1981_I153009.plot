BEGIN PLOT /E288_1981_I153009/d*
END PLOT

BEGIN PLOT /E288_1981_I153009/d09-x01-y01
Title=E288 $\sqrt{s}=27.4 $ GeV
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$5 < M_{\ell\ell} < 6 $ GeV
END PLOT

BEGIN PLOT /E288_1981_I153009/d09-x01-y02
Title=E288 $\sqrt{s}=27.4 $ GeV
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$6 < M_{\ell\ell} < 7 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y03
Title=E288 $\sqrt{s}=27.4 $ GeV
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$7 < M_{\ell\ell} < 8 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y04
Title=E288 $\sqrt{s}=27.4 $ GeV
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$8 < M_{\ell\ell} < 9 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y05
Title=E288 $\sqrt{s}=27.4 $ GeV (Note: resonance region incuded in data)
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$9 < M_{\ell\ell} < 10 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y06
Title=E288 $\sqrt{s}=27.4 $ GeV (Note: resonance region incuded in data)
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$10 < M_{\ell\ell} < 11 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y07
Title=E288 $\sqrt{s}=27.4 $ GeV (Note: resonance region incuded in data)
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$11 < M_{\ell\ell} < 12 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y08
Title=E288 $\sqrt{s}=27.4 $ GeV
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$12 < M_{\ell\ell} < 13 $ GeV
END PLOT
BEGIN PLOT /E288_1981_I153009/d09-x01-y09
Title=E288 $\sqrt{s}=27.4 $ GeV
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LegendTitle=$13 < M_{\ell\ell} < 14 $ GeV
END PLOT

# ... add more histograms as you need them ...
