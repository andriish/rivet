Name: BESIII_2020_I1808166
Year: 2020
Summary: Mass distributions in $D^+\to K^+K^-\pi^+\pi^0$ decays
Experiment: BESIII
Collider: BEPC
InspireID: 1808166
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 102 (2020) 5, 052006
RunInfo: Any process producting D+ mesons, originally psi(3770) decay
Description:
  'Measurement of the mass distributions in $D^+\to K^+K^-\pi^+\pi^0$ decays by the BESIII collaboration. The data were read from the plots in the paper, and then the backgrounds given subtracted, and therefore for some points the error bars are the size of the point. Also resolution and efficiencies have not been unfolded.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2020rxv
BibTeX: '@article{BESIII:2020rxv,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Measurements of the absolute branching fractions of $D^{0(+)}\to K\bar K\pi\pi$ decays}",
    eprint = "2007.10563",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.102.052006",
    journal = "Phys. Rev. D",
    volume = "102",
    number = "5",
    pages = "052006",
    year = "2020"
}
'
