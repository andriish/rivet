BEGIN PLOT /BESIII_2023_I2645388/
Title=$\sigma(e^+e^-\to D^{*0}D^{*-}\pi^++\mathrm{c.c.})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^{*0}D^{*-}\pi^++\mathrm{c.c.})$/pb
ConnectGaps=1
END PLOT
