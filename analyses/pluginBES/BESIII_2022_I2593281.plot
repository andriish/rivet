BEGIN PLOT /BESIII_2022_I2593281/d01-x01-y01
Title=$\sigma(e^+e^-\to pp\bar{p}\bar{n}+\pi^- +\text{c.c.})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to  pp\bar{p}\bar{n}+\pi^- +\text{c.c.})$/fb
ConnectGaps=1
END PLOT
