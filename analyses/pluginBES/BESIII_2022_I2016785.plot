BEGIN PLOT /BESIII_2022_I2016785/d01-x01-y01
Title=$\eta^\prime\eta^\prime$ mass distribution in $J/\psi\to \gamma\eta^\prime\eta^\prime$
XLabel=$m_{\eta^\prime\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2016785/d01-x01-y02
Title=$\gamma\eta^\prime$ mass distribution in $J/\psi\to \gamma\eta^\prime\eta^\prime$
XLabel=$m_{\gamma\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\gamma\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
