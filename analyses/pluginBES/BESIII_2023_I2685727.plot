BEGIN PLOT /BESIII_2023_I2685727/d01-x01-y01
Title=$\sigma(e^+e^-\to K^+K^-\phi)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to K^+K^-\phi)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2685727/d01-x01-y02
Title=$\sigma(e^+e^-\to K^0_SK^0_S\phi)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to K^0_SK^0_S\phi)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
