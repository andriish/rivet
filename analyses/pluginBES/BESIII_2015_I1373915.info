Name: BESIII_2015_I1373915
Year: 2015
Summary: Radiative $J/\psi$ decays to $\pi^0\pi^0$
Experiment: BESIII
Collider: BEPC
InspireID: 1373915
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
- Phys.Rev.D 92 (2015) 5, 052003
RunInfo: Any process producing J/psi, originally e+e-
Description:
  'Measurement of the $\pi^0\pi^0$ mass distribution in the decay $J/\psi\to\gamma\pi^0\pi^0$.
   Plots were read from the paper and are not corrected for efficiency/acceptance,
   although the backgrounds given in the paper were subtracted.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2015rug
BibTeX: '@article{BESIII:2015rug,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis of the $\pi^{0}\pi^{0}$~system produced in radiative $J/\psi$~decays}",
    eprint = "1506.00546",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.92.052003",
    journal = "Phys. Rev. D",
    volume = "92",
    number = "5",
    pages = "052003",
    year = "2015",
    note = "[Erratum: Phys.Rev.D 93, 039906 (2016)]"
}
'
