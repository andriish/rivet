BEGIN PLOT /BESIII_2019_I1725786/d01-x01-y01
Title=$D^0\pi^+$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.42$\,GeV
XLabel=$m_{D^0\pi^+}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d01-x01-y02
Title=$\bar{D}^0\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.42$\,GeV
XLabel=$m_{\bar{D}^0\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\bar{D}^0\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1725786/d02-x01-y01
Title=$D\bar{D}$ mass distribution in $e^+e^-\to D\bar{D}\pi^+\pi^-$ at $\sqrt{s}=4.26$\,GeV
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d02-x01-y02
Title=$D\bar{D}$ mass distribution in $e^+e^-\to D\bar{D}\pi^+\pi^-$ at $\sqrt{s}=4.36$\,GeV
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d02-x01-y03
Title=$D\bar{D}$ mass distribution in $e^+e^-\to D\bar{D}\pi^+\pi^-$ at $\sqrt{s}=4.42$\,GeV
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1725786/d03-x01-y01
Title=$D^0\pi^+\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.36$\,GeV
XLabel=$m_{D^0\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x01-y02
Title=$D^{*+}\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.36$\,GeV
XLabel=$m_{D^{*+}\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^{*+}\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x01-y03
Title=$D^+\pi^+\pi^-$ mass distribution in $e^+e^-\to D^+D^-\pi^+\pi^-$ at $\sqrt{s}=4.36$\,GeV
XLabel=$m_{D^+\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x02-y01
Title=$D^0\pi^+\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.42$\,GeV
XLabel=$m_{D^0\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x02-y02
Title=$D^{*+}\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.42$\,GeV
XLabel=$m_{D^{*+}\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^{*+}\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x02-y03
Title=$D^+\pi^+\pi^-$ mass distribution in $e^+e^-\to D^+D^-\pi^+\pi^-$ at $\sqrt{s}=4.42$\,GeV
XLabel=$m_{D^+\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x03-y01
Title=$D^0\pi^+\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.6$\,GeV
XLabel=$m_{D^0\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x03-y02
Title=$D^{*+}\pi^-$ mass distribution in $e^+e^-\to D^0\bar{D}^0\pi^+\pi^-$ at $\sqrt{s}=4.6$\,GeV
XLabel=$m_{D^{*+}\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^{*+}\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d03-x03-y03
Title=$D^+\pi^+\pi^-$ mass distribution in $e^+e^-\to D^+D^-\pi^+\pi^-$ at $\sqrt{s}=4.6$\,GeV
XLabel=$m_{D^+\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1725786/d04-x01-y01
Title=Cross section for $e^+e^-\to \psi(3770)\pi^+\pi^-$
YLabel=$\sigma(e^+e^-\to \psi(3770)\pi^+\pi^-)$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d06-x01-y01
Title=Cross section for $e^+e^-\to D_1(2420)^0\bar{D}^0$+c.c. ($D_1(2420)^0\to D^0\pi^+\pi^-$)
YLabel=$\sigma(e^+e^-\to D_1(2420)^0\bar{D}^0+\text{c.c.})\times\mathcal{B}(D_1(2420)^0\to D^0\pi^+\pi^-)$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d07-x01-y01
Title=Cross section for $e^+e^-\to D_1(2420)^0\bar{D}^0$+c.c. ($D_1(2420)^0D^{*+}\pi^-$)
YLabel=$\sigma(e^+e^-\to D_1(2420)^0\bar{D}^0+\text{c.c.})\times\mathcal{B}(D_1(2420)^0D^{*+}\pi^-)$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725786/d08-x01-y01
Title=Cross section for $e^+e^-\to D_1(2420)^+D^-$+c.c. ($D_1(2420)^+\to D^+\pi^+\pi^-$)
YLabel=$\sigma(e^+e^-\to D_1(2420)^+D^-+\text{c.c.})\times\mathcal{B}(D_1(2420)^+\to D^+\pi^+\pi^-)$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
