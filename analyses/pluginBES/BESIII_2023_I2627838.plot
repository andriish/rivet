BEGIN PLOT /BESIII_2023_I2627838
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2627838/d01-x01-y01
Title=Amplitude ratio $x=\left|F^0_{1,1}/F^0_{0,0}\right|$ in $\chi_{c0}\to\phi\phi$
YLabel=$x=\left|F^0_{1,1}/F^0_{0,0}\right|$
END PLOT

BEGIN PLOT /BESIII_2023_I2627838/d01-x02-y01
Title=Amplitude ratio $u_1=\left|F^1_{1,0}/F^0_{1,0}\right|$ in $\chi_{c1}\to\phi\phi$
YLabel=$u_1=\left|F^1_{1,0}/F^0_{1,0}\right|$
END PLOT
BEGIN PLOT /BESIII_2023_I2627838/d01-x02-y02
Title=Amplitude ratio $u_2=\left|F^1_{1,1}/F^1_{1,0}\right|$ in $\chi_{c1}\to\phi\phi$
YLabel=$u_2=\left|F^1_{1,1}/F^1_{1,0}\right|$
END PLOT

BEGIN PLOT /BESIII_2023_I2627838/d01-x03-y01
Title=Amplitude ratio $w_1=\left|F^2_{0,1}/F^2_{0,0}\right|$ in $\chi_{c2}\to\phi\phi$
YLabel=$w_1=\left|F^2_{0,1}/F^2_{0,0}\right|$
END PLOT
BEGIN PLOT /BESIII_2023_I2627838/d01-x03-y02
Title=Amplitude ratio $w_2=\left|F^2_{1,-1}/F^2_{0,0}\right|$ in $\chi_{c2}\to\phi\phi$
YLabel=$w_2=\left|F^2_{1,-1}/F^2_{0,0}\right|$
END PLOT
BEGIN PLOT /BESIII_2023_I2627838/d01-x03-y03
Title=Amplitude ratio $w_4=\left|F^2_{1,1}/F^2_{0,0}\right|$ in $\chi_{c2}\to\phi\phi$
YLabel=$w_4=\left|F^2_{1,1}/F^2_{0,0}\right|$
END PLOT

