BEGIN PLOT /BESIII_2019_I1724547/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D_s^+\to \pi^+\pi^0\eta$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1724547/d01-x01-y02
Title=$\pi^+\eta$ mass distribution in $D_s^+\to \pi^+\pi^0\eta$
XLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1724547/d01-x01-y03
Title=$\pi^0\eta$ mass distribution in $D_s^+\to \pi^+\pi^0\eta$
XLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1724547/d01-x01-y04
Title=$\pi^+\eta$ mass distribution in $D_s^+\to \pi^+\pi^0\eta$ with $m(\pi^+\pi^0)>1$\,GeV 
XLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1724547/d01-x01-y05
Title=$\pi^0\eta$ mass distribution in $D_s^+\to \pi^+\pi^0\eta$ with $m(\pi^+\pi^0)>1$\,GeV
XLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1724547/dalitz
Title=Dalitz plot for $D_s^+\to \pi^+\pi^0\eta$
XLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\eta}/{\rm d}m^2_{\pi^0\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
