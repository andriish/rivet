BEGIN PLOT /BESIII_2022_I2166668/d01-x01-y01
Title=$\Lambda\bar\Lambda$ mass distribution in $\chi_{CJ}\to \eta\Lambda\bar\Lambda$
XLabel=$m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166668/d01-x01-y02
Title=$\eta\Lambda$ mass distribution in $\chi_{CJ}\to \eta\Lambda\bar\Lambda$
XLabel=$m_{\eta\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166668/d01-x01-y03
Title=$\eta\bar\Lambda$ mass distribution in $\chi_{CJ}\to \eta\Lambda\bar\Lambda$
XLabel=$m_{\eta\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
