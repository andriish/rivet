BEGIN PLOT /BESIII_2022_I2163048/d01-x01-y01
Title=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$/pb
ConnectGaps=1
END PLOT
