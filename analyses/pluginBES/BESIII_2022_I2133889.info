Name: BESIII_2022_I2133889
Year: 2022
Summary: Cross section for $e^+e^-\to\omega\pi^+\pi^-$ between 2 and 3.08 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 2133889
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2208.04507 [hep-ex]
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams:  [e+,e-]
Energies : [2.0, 2.05, 2.1, 2.125, 2.15, 2.175, 2.2, 2.2324, 2.3094, 2.3864,
            2.396, 2.6444, 2.6464, 2.9, 2.95, 2.981, 3.0, 3.02, 3.08]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\omega\pi^+\pi^-$ between 2 and 3.08 GeV by the BESIII collaboration.
   The cross section for a number of resonant intermediate states are also measured.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022yzp
BibTeX: '@article{BESIII:2022yzp,
    collaboration = "BESIII",
    title = "{Measurement of $e^{+}e^{-}\rightarrow\omega\pi^{+}\pi^{-}$ cross section at $\sqrt s = $ 2.000 to 3.080 GeV}",
    eprint = "2208.04507",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "8",
    year = "2022"
}
'
