BEGIN PLOT /BESIII_2020_I1791570
LogY=0
END PLOT

BEGIN PLOT /BESIII_2020_I1791570/d01-x01-y01
YLabel=$\mu$
XLabel=$\cos\theta_{\Sigma^+}$
Title=$\mu$ for $J/\psi\to\Sigma^+\bar{\Sigma}^-$ (N.B. data not corrected)
END PLOT
BEGIN PLOT /BESIII_2020_I1791570/d01-x01-y02
YLabel=$\mu$
XLabel=$\cos\theta_{\Sigma^+}$
Title=$\mu$ for $\psi(2S)\to\Sigma^+\bar{\Sigma}^-$ (N.B. data not corrected)
END PLOT

BEGIN PLOT /BESIII_2020_I1791570/d02-x01-y01
YMin=-1.2
YMax=-0.8
YLabel=$\alpha_{\Sigma^+\to p \pi^0}$
Title=$\alpha$ for the decay $\Sigma^+\to p \pi^0$
END PLOT
BEGIN PLOT /BESIII_2020_I1791570/d02-x01-y02
YLabel=$\alpha_{\bar\Sigma^-\to \bar{p} \pi^0}$
Title=$\alpha$ for the decay $\bar\Sigma^-\to \bar{p} \pi^0$
END PLOT
BEGIN PLOT /BESIII_2020_I1791570/d02-x01-y03
YLabel=$\alpha_{\bar\Sigma^-\to \bar{p} \pi^0}$
Title=$\alpha$ for the decays $\bar\Sigma^-\to \bar{p} \pi^0$ and  $\bar\Sigma^-\to \bar{p} \pi^0$, averaged
YMax=-0.5
YMin=-1.5
END PLOT

BEGIN PLOT /BESIII_2020_I1791570/d03-x01-y01
YMin=-20
YMax=-10
YLabel=$\Delta\Phi$ [degrees]
Title=$\Delta\Phi$ for $J/\psi\to\Sigma^+\bar{\Sigma}^-$
END PLOT
BEGIN PLOT /BESIII_2020_I1791570/d03-x01-y02
YLabel=$\Delta\Phi$  [degrees]
Title=$\Delta\Phi$ for $\psi(2S)\to\Sigma^+\bar{\Sigma}^-$
END PLOT


BEGIN PLOT /BESIII_2020_I1791570/d04-x01-y01
YMin=-1.
YMax=0.
YLabel=$\alpha_{J\psi}$
Title=$\alpha_{J\psi}$ for $J/\psi\to\Sigma^+\bar{\Sigma}^-$
END PLOT
BEGIN PLOT /BESIII_2020_I1791570/d04-x01-y02
YMin=0
YMax=1.
YLabel=$\alpha_{\psi(2S)}$
Title=$\alpha_{\psi(2S)}$ for $\psi(2S)\to\Sigma^+\bar{\Sigma}^-$
END PLOT
