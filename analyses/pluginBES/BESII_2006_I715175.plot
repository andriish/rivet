BEGIN PLOT /BESII_2006_I715175/d01-x01-y01
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma\omega\omega$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I715175/d01-x01-y02
Title=$\phi_\gamma$ distribution in $J/\psi\to\gamma\omega\omega$
XLabel=$\phi_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I715175/d01-x01-y03
Title=$\cos\theta_\omega$ distribution in $J/\psi\to\gamma\omega\omega$
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I715175/d01-x01-y04
Title=$\phi_\omega$ distribution in $J/\psi\to\gamma\omega\omega$
XLabel=$\phi_\omega$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_\omega$
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I715175/d01-x01-y05
Title=$\cos\theta_\pi$ distribution in $J/\psi\to\gamma\omega\omega$
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\pi$
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I715175/d01-x01-y06
Title=$\chi$ distribution in $J/\psi\to\gamma\omega\omega$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
