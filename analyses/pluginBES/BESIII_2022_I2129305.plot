BEGIN PLOT /BESIII_2022_I2129305/d01-x01-y01
LogY=0
ConnectGaps=1
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
Title=Cross Section for $e^+e^-\to \pi^+\pi^- D^+D^-$
END PLOT
BEGIN PLOT /BESIII_2022_I2129305/d02-x01-y01
LogY=0
ConnectGaps=1
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
Title=Cross Section for $e^+e^-\to \pi^+\pi^- \psi_3(\to D^+D^-)$
END PLOT
