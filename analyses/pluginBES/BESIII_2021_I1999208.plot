BEGIN PLOT /BESIII_2021_I1999208/d01-x01-y01
Title=$\sigma(e^+e^-\to \omega\pi^0\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \omega\pi^0\pi^0)$/pb
LogY=0
ConnectGaps=1
END PLOT
