BEGIN PLOT /BESIII_2018_I1711382/d01-x01-y01
Title=Cross section for $e^+e^-\to D^+_s \bar{D}^{*0} K^-$ +c.c
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1711382/d01-x01-y02
Title=Cross section for $e^+e^-\to D^+_s \bar{D}^0 K^-$ +c.c
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1711382/d01-x01-y03
Title=Cross section for $e^+e^-\to D^+_s D^-_{s1}(2536)(\to\bar{D}^{*0} K^-)$ +c.c
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1711382/d01-x01-y04
Title=Cross section for $e^+e^-\to D^+_s D^-_{s2}(\to\bar{D}^0 K^-)$ +c.c
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1711382/d02-x01-y01
Title=Helicity angle for $D^-_{s2}\to\bar{D}^0 K^-$
XLabel=$|cos\theta|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|cos\theta|$
LogY=0
END PLOT

