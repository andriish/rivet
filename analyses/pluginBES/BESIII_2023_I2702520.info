Name: BESIII_2023_I2702520
Year: 2023
Summary: Cross section for $e^+e^-\to$ $K_S^0K_L^0\pi^0$ for $\sqrt{s}=2.000\to3.080$ GeV
Experiment: BESIII
Collider: <BEPC
InspireID: 2702520
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2309.13883 [hep-ex]
RunInfo: e+e- to hadrons KS0/pi0 stable
NeedCrossSection: yes
Beams: [e+,e-]
Energies: [2.0, 2.05, 2.1, 2.125, 2.15, 2.175, 2.2, 2.2324, 2.3094, 2.3864, 2.396, 2.6444, 2.6464, 2.9, 2.95, 2.981, 3.0, 3.02, 3.08]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to$ $K_S^0K_L^0\pi^0$ for $\sqrt{s}=2.000\to3.080$ GeV by the BESIII
   collaboration.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023xac
BibTeX: '@article{BESIII:2023xac,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Measurement of the $e^{+}e^{-} \to K_{S}^{0} K_{L}^{0} \pi^{0}$ cross sections from $\sqrt{s}=$ 2.000 to 3.080 GeV}",
    eprint = "2309.13883",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "9",
    year = "2023"
}
'
