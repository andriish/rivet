BEGIN PLOT /BESIII_2015_I1382146/d01-x01-y01
Title=Cross section for $e^+e^-\to Z_c(4025)^0\pi^0\to(DD^*)\pi^0$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT

BEGIN PLOT /BESIII_2015_I1382146/d02-x01-y01
Title=Mass distribution for $D^*\bar{D}^*$ average over $\sqrt{s}$
XLabel=$m_{D^*\bar{D}^*}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^*\bar{D}^*}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1382146/d02-x01-y02
Title=Mass distribution for $D^*\bar{D}^*$ at $\sqrt{s}=4.23$ GeV
XLabel=$m_{D^*\bar{D}^*}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^*\bar{D}^*}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1382146/d02-x01-y03
Title=Mass distribution for $D^*\bar{D}^*$ at $\sqrt{s}=4.26$ GeV
XLabel=$m_{D^*\bar{D}^*}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^*\bar{D}^*}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
