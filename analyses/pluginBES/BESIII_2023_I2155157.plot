BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y01
Title=$K^0_SK^0_S\pi^0$ mass in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$m_{K^0_SK^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y02
Title=$K^0_SK^0_S$ mass in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y03
Title=$K^0_S\pi^0$ mass in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y04
Title=$\cos\theta_\gamma$ in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y05
Title=$\cos\theta_{K^0_S}$  in $K^0_SK^0_S\pi^0$ frame in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$\cos\theta_{K^0_S}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^0_S}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y06
Title=$\cos\theta_\pi^0$ in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$\cos\theta_\pi^0$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\pi^0$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2155157/d01-x01-y07
Title=$\cos\theta_{K^0_S}$ in $K^0_SK^0_S$ frame in $J/\psi\to\gamma K^0_SK^0_S\pi^0$
XLabel=$\cos\theta_{K^0_S}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^0_S}$
LogY=0
END PLOT
