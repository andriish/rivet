BEGIN PLOT /BESII_2004_I658084/d01-x01-y01
Title=$K^+K^-$ mass in $J/\psi\to\omega K^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I658084/d01-x01-y02
Title=$\omega K$ mass in $J/\psi\to\omega K^+K^-$
XLabel=$m_{\omega K}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}m_{\omega K}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I658084/d02-x01-y01
Title=Angle between decay planes in $J/\psi\to\omega K^+K^-$ ($m_{K^+K^+}>2\,$GeV)
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\chi$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I658084/d02-x01-y02
Title=$\omega$ polar angle in $J/\psi\to\omega K^+K^-$ ($m_{K^+K^+}>2\,$GeV)
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I658084/d02-x01-y03
Title=Kaon decay angle in $J/\psi\to\omega K^+K^-$ ($m_{K^+K^+}>2\,$GeV)
XLabel=$\cos\alpha_K$
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\alpha_K$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I658084/d02-x01-y04
Title=$\omega$ decay plane angle in $J/\psi\to\omega K^+K^-$ ($m_{K^+K^+}>2\,$GeV)
XLabel=$\cos\beta_\omega$
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\beta_\omega$
LogY=0
END PLOT
