Name: BESIII_2019_I1725786
Year: 2019
Summary: $e^+e^-\to D\bar{D}\pi^+\pi^-$ for $\sqrt{s}=4.0854$ to $4.6$ GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1725786
Status: VALIDATED NOHEPDATA SINGLEWEIGHT
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 100 (2019) 3, 032005
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Energies: [4.0854, 4.1886, 4.2077, 4.2171, 4.2263, 4.2417, 4.258, 4.3079, 4.3583, 4.3874, 4.4156, 4.4671, 4.5271, 4.5745, 4.5995]
Options:
- ENERGY=*
Description:
'Analysis of the resonant contributions to  $e^+e^-\to D\bar{D}\pi^+\pi^-$ for $\sqrt{s}=4.0854$ to $4.6$ GeV,
where $D\bar{D}=D^+D^-, D^0\bar{D}^0$. The cross sections for the production of $\pi^+\pi^-\psi(3770)$ and
$D_1(2420^0\bar{D}^0$+c.c. are measured as a function of energy and the mass distrinutions are implemented for
three energies.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2019tdo
BibTeX: '@article{BESIII:2019tdo,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of $e^{+}e^{-}\rightarrow \pi^{+}\pi^{-}\psi(3770)$ and $D_{1}(2420)^{0}\bar{D}^{0} + c.c.$}",
    eprint = "1903.08126",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.100.032005",
    journal = "Phys. Rev. D",
    volume = "100",
    number = "3",
    pages = "032005",
    year = "2019"
}
'
