BEGIN PLOT /BESIII_2021_I1921775/d01-x01-y01
Title=Angular distribution for $e^+e^-\to \psi(2S)\to\Xi^{*0}\bar{\Xi}^{*0}$
XLabel=$\cos\theta_{e-\Xi^{*0}}$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_{e-\Xi^{*0}}$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2021_I1921775/d01-x01-y02
Title=Angular distribution for $e^+e^-\to \psi(2S)\to\Xi^{*0}\bar{\Xi}^{*0}$
XLabel=$\cos\theta_{e-\Xi^{*0}}$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_{e-\Xi^{*0}}$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2021_I1921775/d02-x01-y01
Title=$\alpha$ parameter for $e^+e^-\to \psi(2S)\to\Xi^{*0}\bar{\Xi}^{*0}$
XLabel=
YLabel=$\alpha$
LogY=0
END PLOT
