Name: BESIII_2019_I1731057
Year: 2019
Summary:  Dalitz plot analysis of $J/\psi\to K^+K^-\pi^0$
Experiment: BESIII
Collider: BEPC
InspireID: 1731057
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 100 (2019) 3, 032004
RunInfo: Any process producing J/psi, originally e+e-
Description:
  'Measurement of the mass distributions in the decays $J/\psi\to K^+K^-\pi^0$ by BESIII. The data were read from the plots in the paper and therefore for most points the error bars are the size of the point. It is also not clear that any resolution/acceptance effects have been unfolded.'
ValidationInfo:
  'Herwig7 events uing BABAR model'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2019apb
BibTeX: '@article{BESIII:2019apb,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Partial-wave analysis of $J/\psi\to K^+K^-\pi^0$}",
    eprint = "1904.10630",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.100.032004",
    journal = "Phys. Rev. D",
    volume = "100",
    number = "3",
    pages = "032004",
    year = "2019"
}
'
