BEGIN PLOT /BESIII_2022_I2038523/d01-x01-y01
Title=$\pi^0\pi^0$ mass in $D^+_s\to \pi^0\pi^0 e^+\nu_e$
XLabel=$m_{\pi^0\pi^0}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
