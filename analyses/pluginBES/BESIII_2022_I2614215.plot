BEGIN PLOT /BESIII_2022_I2614215/d01-x01-y01
Title=Cross section for $e^+e^-\to \omega X(3872)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \omega X(3872)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
