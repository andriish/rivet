BEGIN PLOT /BESIII_2022_I2513076/d01
LogY=0
XLabel=$p_{\pi^0}$ [GeV]
YLabel=$1/\sigma_{\text{had}} \text{d}\sigma/\text{d}p_{\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d02
LogY=0
XLabel=$p_{K^0_S}$ [GeV]
YLabel=$1/\sigma_{\text{had}} \text{d}\sigma/\text{d}p_{K^0_S}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d01-x01-y01
Title=$\pi^0$ momentum spectrum for $\sqrt{s}=2.2324\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d01-x01-y02
Title=$\pi^0$ momentum spectrum for $\sqrt{s}=2.4\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d01-x01-y03
Title=$\pi^0$ momentum spectrum for $\sqrt{s}=2.8\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d01-x01-y04
Title=$\pi^0$ momentum spectrum for $\sqrt{s}=3.05\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d01-x01-y05
Title=$\pi^0$ momentum spectrum for $\sqrt{s}=3.4\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d01-x01-y06
Title=$\pi^0$ momentum spectrum for $\sqrt{s}=3.671\,$GeV
END PLOT

BEGIN PLOT /BESIII_2022_I2513076/d02-x01-y01
Title=$K^0_S$ momentum spectrum for $\sqrt{s}=2.2324\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d02-x01-y02
Title=$K^0_S$ momentum spectrum for $\sqrt{s}=2.4\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d02-x01-y03
Title=$K^0_S$ momentum spectrum for $\sqrt{s}=2.8\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d02-x01-y04
Title=$K^0_S$ momentum spectrum for $\sqrt{s}=3.05\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d02-x01-y05
Title=$K^0_S$ momentum spectrum for $\sqrt{s}=3.4\,$GeV
END PLOT
BEGIN PLOT /BESIII_2022_I2513076/d02-x01-y06
Title=$K^0_S$ momentum spectrum for $\sqrt{s}=3.671\,$GeV
END PLOT
