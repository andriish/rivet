BEGIN PLOT /BESIII_2020_I1836509/d01-x01-y01
Title=$\sigma(e^+e^-\to \eta^\prime\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \eta^\prime\pi^+\pi^-)$/pb
ConnectGaps=1
END PLOT
