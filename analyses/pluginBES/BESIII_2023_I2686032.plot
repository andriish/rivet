BEGIN PLOT /BESIII_2023_I2686032/d01-x01-y01
Title=$\sigma(e^+e^-\to \Lambda\bar{\Sigma}^0+\mathrm{c.c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Lambda\bar{\Sigma}^0+\mathrm{c.c.})$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2686032/d01-x01-y02
Title=$F(s)$ for $e^+e^-\to \Lambda\bar{\Sigma}^0+\mathrm{c.c}$
XLabel=$\sqrt{s}$/GeV
YLabel=$F(s)$
ConnectGaps=1
END PLOT
