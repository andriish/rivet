BEGIN PLOT /BESIII_2017_I1505590/d01-x01-y01
Title=$K^\pm\pi^0$ mass distribution in $\chi_{c2}\to K^+K^-\pi^0$
XLabel=$m_{K^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1505590/d01-x01-y02
Title=$K^+K^-$ mass distribution in $\chi_{c2}\to K^+K^-\pi^0$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2017_I1505590/d02-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $\chi_{c2}\to K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1505590/d02-x01-y02
Title=$K^0_S\pi^\mp$ mass distribution in $\chi_{c2}\to K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_S\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1505590/d02-x01-y03
Title=$K^0_SK^\pm$ mass distribution in $\chi_{c2}\to K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_SK^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1505590/d02-x01-y04
Title=$\pi^\pm\pi^0$ mass distribution in $\chi_{c2}\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
