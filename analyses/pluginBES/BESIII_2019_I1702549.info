Name: BESIII_2019_I1702549
Year: 2019
Summary:  $D_s^+\to K^0 e^+\nu_e$ $q^2$ spectrum $D_s^+\to K^{*0} e^+\nu_e$ $q^2$ distributions
Experiment: BESIII
Collider: BEPC
InspireID: 1702549
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 122 (2019) 6, 061801
RunInfo: Any process producing D_s+
Description:
  'Measurement of the $q^2$ spectrum in the decay $D_s^+\to K^0 e^+\nu_e$ and kinematic distributions in $D_s^+\to K^{*0} e^+\nu_e$ $q^2$ by BES-III. N.B. the plots where read from the paper and may not have been corrected for acceptance.'
ValidationInfo:
  'Herwig 7 events using EvtGen for the decays.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Ablikim:2018upe
BibTeX: '@article{Ablikim:2018upe,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{First Measurement of the Form Factors in $D^+_{s}\rightarrow K^0 e^+\nu_e$ and $D^+_{s}\rightarrow K^{*0} e^+\nu_e$ Decays}",
    eprint = "1811.02911",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.122.061801",
    journal = "Phys. Rev. Lett.",
    volume = "122",
    number = "6",
    pages = "061801",
    year = "2019"
}'
