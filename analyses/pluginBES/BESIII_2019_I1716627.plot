BEGIN PLOT /BESIII_2019_I1716627/d01-x01-y01
Title=$2K^0_S$ mass distribution in $\chi_{cJ}\to 4K^0_S$
XLabel=$m_{2K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{2K^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1716627/d01-x01-y02
Title=$3K^0_S$ mass distribution in $\chi_{cJ}\to 4K^0_S$
XLabel=$m_{3K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{3K^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
