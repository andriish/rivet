BEGIN PLOT /BESIII_2022_I2030993/d01-x01-y01
Title=$\pi^+\eta^\prime$ mass distribution in $D_s^+\to \pi^+\pi^0\eta^\prime$
XLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2030993/d01-x01-y02
Title=$\pi^0\eta^\prime$ mass distribution in $D_s^+\to \pi^+\pi^0\eta^\prime$
XLabel=$m^2_{\pi^0\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2030993/d01-x01-y03
Title=$\pi^+\pi^0$ mass distribution in $D_s^+\to \pi^+\pi^0\eta^\prime$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2030993/dalitz
Title=Dalitz plot for $D_s^+\to \pi^+\pi^0\eta^\prime$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\pi^0}/{\rm d}m^2_{\pi^+\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
