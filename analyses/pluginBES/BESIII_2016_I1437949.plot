BEGIN PLOT /BESIII_2016_I1437949
XLabel=$m_{\pi^+\pi^-\eta^\prime}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\eta^\prime}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1437949/d01-x01-y01
Title=$\pi^+\pi^-\eta^\prime$ mass distribution in $J/\psi\to\pi^+\pi^-\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
END PLOT
BEGIN PLOT /BESIII_2016_I1437949/d01-x01-y02
Title=$\pi^+\pi^-\eta^\prime$ mass distribution in $J/\psi\to\pi^+\pi^-\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
END PLOT
BEGIN PLOT /BESIII_2016_I1437949/d02-x01-y01
Title=$\pi^+\pi^-\eta^\prime$ mass distribution in $J/\psi\to\pi^+\pi^-\eta^\prime$
END PLOT
