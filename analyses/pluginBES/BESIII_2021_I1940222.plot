BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+\omega$
XLabel=$m_{K^-\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y02
Title=$K^-\omega$ mass distribution in $D^0\to K^-\pi^+\omega$
XLabel=$m_{K^-\omega}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\omega}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y03
Title=$\pi^+\omega$ mass distribution in $D^0\to K^-\pi^+\omega$
XLabel=$m_{\pi^+\omega}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\omega}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/dalitz_1
Title=Dalitz plot for $D^0\to K^-\pi^+\omega$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\omega}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{\pi^+\omega}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT


BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y04
Title=$K^0_S\pi^0$ mass distribution in $D^0\to K^0_S\pi^0\omega$
XLabel=$m_{K^0_S\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y05
Title=$K^0_S\omega$ mass distribution in $D^0\to K^0_S\pi^0\omega$
XLabel=$m_{K^0_S\omega}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\omega}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y06
Title=$\pi^0\omega$ mass distribution in $D^0\to K^0_S\pi^0\omega$
XLabel=$m_{\pi^0\omega}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\omega}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/dalitz_2
Title=Dalitz plot for $D^0\to K^0_S\pi^0\omega$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^0\omega}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^0}/{\rm d}m^2_{\pi^0\omega}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y07
Title=$K^0_S\pi^+$ mass distribution in $D^+\to K^0_S\pi^+\omega$
XLabel=$m_{K^0_S\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y08
Title=$K^0_S\omega$ mass distribution in $D^+\to K^0_S\pi^+\omega$
XLabel=$m_{K^0_S\omega}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\omega}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/d01-x01-y09
Title=$\pi^+\omega$ mass distribution in $D^+\to K^0_S\pi^+\omega$
XLabel=$m_{\pi^+\omega}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\omega}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1940222/dalitz_3
Title=Dalitz plot for $D^+\to K^0_S\pi^+\omega$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\omega}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^+}/{\rm d}m^2_{\pi^+\omega}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
