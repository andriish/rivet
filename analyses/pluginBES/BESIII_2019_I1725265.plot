BEGIN PLOT /BESIII_2019_I1725265/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+\pi^0\pi^0$
XLabel=$m^2_{K^-\pi^+}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^-\pi^+}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725265/d01-x01-y02
Title=$K^-\pi^0$ mass distribution in $D^0\to K^-\pi^+\pi^0\pi^0$
XLabel=$m^2_{K^-\pi^0}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^-\pi^0}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725265/d01-x01-y03
Title=$\pi^+\pi^0$ mass distribution in $D^0\to K^-\pi^+\pi^0\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^0}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1725265/d01-x01-y04
Title=$\pi^0\pi^0$ mass distribution in $D^0\to K^-\pi^+\pi^0\pi^0$
XLabel=$m^2_{\pi^0\pi^0}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^0\pi^0}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
