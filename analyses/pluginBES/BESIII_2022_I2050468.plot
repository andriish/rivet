BEGIN PLOT /BESIII_2022_I2050468/d01-x01-y01
LogY=0
ConnectGaps=1
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
Title=Cross Section for $e^+e^-\to \pi^+\pi^- \psi_2(\to \gamma\chi_{c1})$
END PLOT
