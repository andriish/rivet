BEGIN PLOT /BESII_2006_I650381/d01-x01-y01
Title=$p\pi^-$ mass distribution in $J/\psi\to p\bar{n}\pi^-$
XLabel=$m_{p\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I650381/d01-x01-y02
Title=$\bar{p}\pi^+$ mass distribution in $J/\psi\to n\bar{p}\pi^+$
XLabel=$m_{\bar{p}\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I650381/dalitz_1
Title=Dalitz plot for  $J/\psi\to p\bar{n}\pi^-$
XLabel=$m^2_{p\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{n}\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\pi^-}/{\rm d}m^2_{\bar{n}\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I650381/dalitz_2
Title=Dalitz plot for  $J/\psi\to n\bar{p}\pi^+$
XLabel=$m^2_{\bar{p}\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{n\pi^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\bar{p}\pi^+}/{\rm d}m^2_{n\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
