BEGIN PLOT /BES_1999_I507639/d01-x01-y01
Title=$2\pi^+2\pi^-$ mass distribution in $J/\psi\to\gamma 2\pi^+2\pi^-$
XLabel=$m_{2\pi^+2\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^+2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
