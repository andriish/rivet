BEGIN PLOT /BESIII_2020_I1793431/
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d01-x01-y01
Title=Cross section for $e^+e^-\to J/\psi\pi^0\pi^0$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d01-x01-y02
Title=Ratio of cross section for $e^+e^-\to J/\psi\pi^0\pi^0$ to  $e^+e^-\to J/\psi\pi^+\pi^-$
YLabel=$R$
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d03-x01-y01
Title=Cross section for $e^+e^-\to Z_c(3900)^0\pi^0$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
END PLOT

BEGIN PLOT /BESIII_2020_I1793431/d02-x01-y01
Title=Mass distribution for $J/\psi\pi^0$ at $\sqrt{s}=4.226$ GeV
XLabel=$m_{J/\psi\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d02-x01-y02
Title=Mass distribution for $\pi^0\pi^0$ at $\sqrt{s}=4.226$ GeV
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2020_I1793431/d02-x02-y01
Title=Mass distribution for $J/\psi\pi^0$ at $\sqrt{s}=4.236$ GeV
XLabel=$m_{J/\psi\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d02-x02-y02
Title=Mass distribution for $\pi^0\pi^0$ at $\sqrt{s}=4.236$ GeV
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2020_I1793431/d02-x03-y01
Title=Mass distribution for $J/\psi\pi^0$ at $\sqrt{s}=4.244$ GeV
XLabel=$m_{J/\psi\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d02-x03-y02
Title=Mass distribution for $\pi^0\pi^0$ at $\sqrt{s}=4.244$ GeV
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2020_I1793431/d02-x04-y01
Title=Mass distribution for $J/\psi\pi^0$ at $\sqrt{s}=4.258$ GeV
XLabel=$m_{J/\psi\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2020_I1793431/d02-x04-y02
Title=Mass distribution for $\pi^0\pi^0$ at $\sqrt{s}=4.258$ GeV
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
