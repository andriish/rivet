BEGIN PLOT /BESIII_2018_I1681638/d01-x01-y01
Title=Cross Section for $e^+e^-\to p K^0_S \bar{n} K^-$+c.c.
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2018_I1681638/d01-x01-y02
Title=Cross Section for $e^+e^-\to \Lambda(1520)^0 \bar{n} K^0_S$+c.c.
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2018_I1681638/d01-x01-y03
Title=Cross Section for $e^+e^-\to \Lambda(1520)^0 \bar{p} K^+$+c.c.
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
