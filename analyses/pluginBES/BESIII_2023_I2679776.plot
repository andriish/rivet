BEGIN PLOT /BESIII_2023_I2679776/d01-x01-y01
Title=$D_s^+\to \eta \mu^+ \nu_\mu$ (using $\eta\to\gamma\gamma$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2679776/d01-x01-y02
Title=$D_s^+\to \eta \mu^+ \nu_\mu$ (using $\eta\to\pi^0\pi^+\pi^-$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2023_I2679776/d02-x01-y01
Title=$D_s^+\to \eta^\prime \mu^+ \nu_\mu$ (using $\eta^\prime\to\eta \pi^+\pi^-$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2679776/d02-x01-y02
Title=$D_s^+\to \eta^\prime \mu^+ \nu_\mu$ (using $\eta^\prime\to\gamma\rho$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT
