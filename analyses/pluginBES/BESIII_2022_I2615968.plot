BEGIN PLOT /BESIII_2022_I2615968/d01-x01-y01
Title=$K^0_S\pi^+$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2615968/d01-x01-y02
Title=$K^0_S\pi^-$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2615968/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2615968/d02-x01-y01
Title=$K^0_L\pi^+$ mass distribution in $D^0\to K^0_L\pi^+\pi^-$
XLabel=$m^2_{K^0_L\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_L\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2615968/d02-x01-y02
Title=$K^0_L\pi^-$ mass distribution in $D^0\to K^0_L\pi^+\pi^-$
XLabel=$m^2_{K^0_L\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_L\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2615968/d02-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K^0_L\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
