Name: BESIII_2023_I2693763
Year: 2023
Summary: Mads distributions in $\psi(2S)\to\phi K^0_SK^0_S$
Experiment: BESIII
Collider: BEPC
InspireID: 2693763
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 108 (2023) 5, 052001
 - arXiv:2303.08317 [hep-ex]
RunInfo: Any process producing psi(2S), originally e+e-
Description:
  'Measurement of mass distributions in $\psi(2S)\to\phi K^0_SK^0_S$ by BES. N.B. data read from plots and may not be corrected, no difference between $K_{S1}^0$ and $K^0_{S2}$ so these are symeterised.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023pgb
BibTeX: '@article{BESIII:2023pgb,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of the Branching Fraction for the Decay $\psi(3686) \rightarrow \phi K_{S}^{0} K_{S}^{0}$}",
    eprint = "2303.08317",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "3",
    year = "2023"
}
'
