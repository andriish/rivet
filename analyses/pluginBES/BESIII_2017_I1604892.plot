BEGIN PLOT /BESIII_2017_I1604892/d01-x01-y01
Title=$\pi^+\pi^-$ mass for $e^+e^-\to\pi^+\pi^-J/\psi$ at $\sqrt{s}=4.23$GeV
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1604892/d01-x01-y02
Title=$J/\psi\pi^\pm$ mass for $e^+e^-\to\pi^+\pi^-J/\psi$ at $\sqrt{s}=4.23$GeV
XLabel=$m_{J/\psi\pi^\pm}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^\pm}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1604892/d01-x02-y01
Title=$\pi^+\pi^-$ mass for $e^+e^-\to\pi^+\pi^-J/\psi$ at $\sqrt{s}=4.26$GeV
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1604892/d01-x02-y02
Title=$J/\psi\pi^\pm$ mass for $e^+e^-\to\pi^+\pi^-J/\psi$ at $\sqrt{s}=4.26$GeV
XLabel=$m_{J/\psi\pi^\pm}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^\pm}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1604892/d02-x01-y01
Title=Helicity angle for $Z_c^\pm\to\pi^\pm J/\psi$
XLabel=$|\cos\theta|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|cos\theta|$
LogY=0
END PLOT
