BEGIN PLOT /BABAR_2005_I686355/d01-x01-y01
Title=Polarization in $B^0\to D^{*+}D^{*-}$
YLabel=$R_\perp$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2005_I686355/d02-x01-y01
Title=$\cos\theta_\text{tr}$ in $B^0\to D^{*+}D^{*-}$
XLabel=$\cos\theta_\text{tr}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\text{tr}$
LogY=0
END PLOT
