Name: BABAR_2004_I626730
Year: 2004
Summary: Mass and helicity angles in $B^+\to K^+\pi^+\pi^-$
Experiment: BABAR
Collider: PEP-II
InspireID: 626730
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 70 (2004) 092001
RunInfo: Any process producing B+ mesons, originally Upsilon(4S) decays
Description:
'Measurement of mass distributions and helicity angles in $B^+\to K^+\pi^+\pi^-$.
 The efficiency corrected, background subtracted data were read from the figures in the paper'
ValidationInfo:
  'Herwig7 events using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2003zav
BibTeX: '@article{BaBar:2003zav,
    author = "Aubert, Bernard and others",
    collaboration = "BaBar",
    title = "{Measurements of the branching fractions of charged $B$ decays to $K^\pm \pi^\mp \pi^\pm$ final states}",
    eprint = "hep-ex/0308065",
    archivePrefix = "arXiv",
    reportNumber = "SLAC-PUB-10141, BABAR-PUB-03-027",
    doi = "10.1103/PhysRevD.70.092001",
    journal = "Phys. Rev. D",
    volume = "70",
    pages = "092001",
    year = "2004"
}
'
