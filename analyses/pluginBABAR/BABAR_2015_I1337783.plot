BEGIN PLOT /BABAR_2015_I1337783/d01-x01-y01
Title=CP asymmetry in $\bar{B}\to X_{s+d}\gamma$
XLabel=$E^{*,\min}_\gamma$
YLabel=$A_{CP}$ [$\%$]
LogY=0
END PLOT
