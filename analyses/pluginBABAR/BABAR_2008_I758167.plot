BEGIN PLOT /BABAR_2008_I758167/d01-x01-y0
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I758167/d01-x01-y01
Title=Helicity angle for $B\to\bar{D}D_{s1}^+(2536)$
END PLOT
BEGIN PLOT /BABAR_2008_I758167/d01-x01-y02
Title=Helicity angle for $B\to\psi(3770)K$
END PLOT
BEGIN PLOT /BABAR_2008_I758167/d01-x01-y03
Title=Helicity angle for $B\to X(3872)K$
END PLOT
