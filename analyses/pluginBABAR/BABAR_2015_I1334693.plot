# BEGIN PLOT /BABAR_2015_I1334693/d01-x01-y01
Title=$D^0\to\pi^- e^+ \nu_e$
XLabel=$q^2$~[GeV$^2$]
#YLabel=[Uncomment and insert y-axis label for histogram d01-x01-y01 here]
NormalizeToIntegral=1
# END PLOT

# ... add more histograms as you need them ...
