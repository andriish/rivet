BEGIN PLOT /BABAR_2008_I766290/d01-x01-y01
Title=$\Lambda_c^+K^-$ mass in  $B^-\to\Lambda_c^+\bar{\Lambda}_c^-K^-$
XLabel=$m_{\Lambda_c^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Lambda_c^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I766290/d01-x01-y02
Title=$\Lambda_c^+\bar{\Lambda}_c^-$ mass in  $B^-\to\Lambda_c^+\bar{\Lambda}_c^-K^-$
XLabel=$m_{\Lambda_c^+\bar{\Lambda}_c^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Lambda_c^+\bar{\Lambda}_c^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
