BEGIN PLOT /BABAR_2012_I1086537/d01-x01-y01
Title=Lower $K^+K^-$ mass distribution in $B^+\to K^+K^-K^+$+c.c.
XLabel=$m_{K^+K^-,\text{low}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-,\text{low}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d01-x01-y02
Title=Higher $K^+K^-$ mass distribution in $B^+\to K^+K^-K^+$+c.c.
XLabel=$m_{K^+K^-,\text{high}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-,\text{high}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d01-x01-y03
Title=$K^+K^+$ mass distribution in $B^+\to K^+K^-K^+$+c.c.
XLabel=$m_{K^+K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d02-x01-y01
Title=Lower $K^+K^-$ mass distribution in $B^+\to K^+K^-K^+$
XLabel=$m_{K^+K^-,\text{low}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-,\text{low}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d02-x01-y02
Title=Lower $K^+K^-$ mass distribution in $B^-\to K^-K^-K^+$
XLabel=$m_{K^+K^-,\text{low}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-,\text{low}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d02-x02-y01
Title=Higher $K^+K^-$ mass distribution in $B^+\to K^+K^-K^+$
XLabel=$m_{K^+K^-,\text{high}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-,\text{high}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d02-x02-y02
Title=Higher $K^+K^-$ mass distribution in $B^-\to K^-K^-K^+$
XLabel=$m_{K^+K^-,\text{high}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-,\text{high}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I1086537/d03-x01-y01
Title=$K^0_SK^0_S$ mass distribution in $B^+\to K^0_SK^0_SK^+$+c.c.
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d03-x01-y02
Title=Higher $K^0_SK^+$ mass distribution in $B^+\to K^0_SK^0_SK^+$+c.c.
XLabel=$m_{K^0_SK^+,\text{high}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^+,\text{high}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d03-x01-y03
Title=Lower $K^0_SK^+$ mass distribution in $B^+\to K^0_SK^0_SK^+$+c.c.
XLabel=$m_{K^0_SK^+,\text{low}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^+,\text{low}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I1086537/d04-x01-y01
Title=$K^0_SK^0_S$ mass distribution in $B^+\to K^0_SK^0_SK^+$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d04-x01-y02
Title=$K^0_SK^0_S$ mass distribution in $B^-\to K^0_SK^0_SK^-$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I1086537/d05-x01-y01
Title=$K^0_SK^0_S$ mass distribution in $B^0\to K^+K^-K^0_S$+c.c.
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d05-x01-y02
Title=Higher $K^0_SK^+$ mass distribution in $B^0\to K^+K^-K^0_S$+c.c.
XLabel=$m_{K^0_SK^+,\text{high}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^+,\text{high}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1086537/d05-x01-y03
Title=Lower $K^0_SK^+$ mass distribution in $B^0\to K^+K^-K^0_S$+c.c.
XLabel=$m_{K^0_SK^+,\text{low}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^+,\text{low}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
