BEGIN PLOT /BABAR_2006_I714147/d01-x01-y01
Title=Pion helicity angle for $B^-\to D^0 K^{*-}$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
