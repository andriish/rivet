BEGIN PLOT /BABAR_2012_I1107905/d01-x01-y01
Title=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$/pb
ConnectGaps=1
LogY=0
END PLOT
