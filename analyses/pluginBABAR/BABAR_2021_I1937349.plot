BEGIN PLOT /BABAR_2021_I1937349/d01-x01-y01
Title=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0)$/nb
END PLOT
