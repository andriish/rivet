BEGIN PLOT /BABAR_2014_I1287632/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\eta$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2014_I1287632/d01-x01-y02
Title=$K^+\eta$ mass distribution in $\eta_c\to K^+K^-\eta$
XLabel=$m^2_{K^+\eta}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2014_I1287632/d01-x01-y03
Title=$K^-\eta$ mass distribution in $\eta_c\to K^+K^-\eta$
XLabel=$m^2_{K^-\eta}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2014_I1287632/dalitz_1
Title=Dalitz plot for  $\eta_c\to K^+K^-\eta$
XLabel=$m^2_{K^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\eta}/{\rm d}m^2_{K^-\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2014_I1287632/d02-x01-y01
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2014_I1287632/d02-x01-y02
Title=$K^+\pi^0$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2014_I1287632/d02-x01-y03
Title=$K^-\pi^0$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2014_I1287632/dalitz_2
Title=Dalitz plot for  $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^0}/{\rm d}m^2_{K^-\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
