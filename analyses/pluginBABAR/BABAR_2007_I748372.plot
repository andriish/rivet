BEGIN PLOT /BABAR_2007_I748372/d01
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x01-y01
Title=Kaon helicity angle for $B^0\to J/\psi K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x01-y02
Title=Kaon helicity angle for $B^+\to J/\psi K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x01-y03
Title=Kaon helicity angle for $B^+\to J/\psi K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x02-y01
Title=Kaon helicity angle for $B^0\to \psi(2S) K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x02-y02
Title=Kaon helicity angle for $B^+\to \psi(2S) K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x02-y03
Title=Kaon helicity angle for $B^+\to \psi(2S) K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x03-y01
Title=Kaon helicity angle for $B^0\to \chi_{c1} K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x03-y02
Title=Kaon helicity angle for $B^+\to \chi_{c1} K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d01-x03-y03
Title=Kaon helicity angle for $B^+\to \chi_{c1} K^{*+}(\to K^+\pi^0)$
END PLOT

BEGIN PLOT /BABAR_2007_I748372/d02
XLabel=$\cos\theta_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\text{tr}}$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x01-y01
Title=$\cos\theta_{\text{tr}}$ for $B^0\to J/\psi K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x01-y02
Title=$\cos\theta_{\text{tr}}$ for $B^+\to J/\psi K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x01-y03
Title=$\cos\theta_{\text{tr}}$ for $B^+\to J/\psi K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x02-y01
Title=$\cos\theta_{\text{tr}}$ for $B^0\to \psi(2S) K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x02-y02
Title=$\cos\theta_{\text{tr}}$ for $B^+\to \psi(2S) K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x02-y03
Title=$\cos\theta_{\text{tr}}$ for $B^+\to \psi(2S) K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x03-y01
Title=$\cos\theta_{\text{tr}}$ for $B^0\to \chi_{c1} K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x03-y02
Title=$\cos\theta_{\text{tr}}$ for $B^+\to \chi_{c1} K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d02-x03-y03
Title=$\cos\theta_{\text{tr}}$ for $B^+\to \chi_{c1} K^{*+}(\to K^+\pi^0)$
END PLOT

BEGIN PLOT /BABAR_2007_I748372/d03
XLabel=$\phi_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_{\text{tr}}$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x01-y01
Title=$\phi_{\text{tr}}$ for $B^0\to J/\psi K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x01-y02
Title=$\phi_{\text{tr}}$ for $B^+\to J/\psi K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x01-y03
Title=$\phi_{\text{tr}}$ for $B^+\to J/\psi K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x02-y01
Title=$\phi_{\text{tr}}$ for $B^0\to \psi(2S) K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x02-y02
Title=$\phi_{\text{tr}}$ for $B^+\to \psi(2S) K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x02-y03
Title=$\phi_{\text{tr}}$ for $B^+\to \psi(2S) K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x03-y01
Title=$\phi_{\text{tr}}$ for $B^0\to \chi_{c1} K^{*0}(\to K^+\pi^-)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x03-y02
Title=$\phi_{\text{tr}}$ for $B^+\to \chi_{c1} K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /BABAR_2007_I748372/d03-x03-y03
Title=$\phi_{\text{tr}}$ for $B^+\to \chi_{c1} K^{*+}(\to K^+\pi^0)$
END PLOT
