# BEGIN PLOT /UA5_1987_I244829/d01-x01-y01
Title=Mean charged multiplicity at $\sqrt{s} = 546~\mathrm{GeV}$, $|\eta| < 5.0$
XLabel=$\mathrm{GeV}$
YLabel=$N_\mathrm{ch}$
# END PLOT

# BEGIN PLOT /UA5_1987_I244829/d03-x01-y01
Title=Charged multiplicity at $\sqrt{s} = 546~\mathrm{GeV}$, $|\eta| < 5.0$
XLabel=$N_\mathrm{ch}$
YLabel=$\mathrm{d}{\sigma}/\mathrm{d}{N_\mathrm{ch}}$
# END PLOT
