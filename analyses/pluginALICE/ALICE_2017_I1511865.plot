BEGIN PLOT /ALICE_2017_I1511865/d01-x01-y01
Title=J/$\psi$ transverse momentum $2.5<y<4.0$ $\sqrt{s}=13$\,TeV
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ALICE_2017_I1511865/d02-x01-y01
Title=J/$\psi$ rapidity $2.5<y<4.0$ $\sqrt{s}=13$\,TeV
XLabel=$y^{J/\psi}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
LogY=0
END PLOT

BEGIN PLOT /ALICE_2017_I1511865/d03-x01-y01
Title=$\psi(2S)$ $2.5<y<4.0$ $\sqrt{s}=13$\,TeV
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ALICE_2017_I1511865/d04-x01-y01
Title=$\psi(2S)$ $2.5<y<4.0$ $\sqrt{s}=13$\,TeV
XLabel=$y^{\psi(2S)}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
LogY=0
END PLOT

BEGIN PLOT /ALICE_2017_I1511865/d05-x01-y01
Title=Ratio of $\psi(2S)/J/\psi$ vs $p_\perp$ $2.5<y<4.0$ $\sqrt{s}=13$\,TeV
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma_{\psi(2S)}/\mathrm{d}p_\perp\mathrm{d}y/\mathrm{d}^2\sigma_{J/\psi}/\mathrm{d}p_\perp\mathrm{d}y$ 
LogY=0
END PLOT
BEGIN PLOT /ALICE_2017_I1511865/d06-x01-y01
Title=Ratio of $\psi(2S)/J/\psi$ vs rapidity $2.5<y<4.0$ $\sqrt{s}=13$\,TeV
XLabel=$y$
YLabel=$\mathrm{d}\sigma_{\psi(2S)}/\mathrm{d}y/\mathrm{d}\sigma_{J/\psi}/\mathrm{d}y$ 
LogY=0
END PLOT

BEGIN PLOT /ALICE_2017_I1511865/d07-x01-y01
Title=J/$\psi$ transverse momentum $2.5<y<4.0$ $\sqrt{s}=5.02$\,TeV
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /ALICE_2017_I1511865/d08-x01-y01
Title=J/$\psi$ rapidity $2.5<y<4.0$ $\sqrt{s}=5.02$\,TeV
XLabel=$y^{J/\psi}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
LogY=0
END PLOT
