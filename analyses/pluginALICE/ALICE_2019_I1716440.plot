BEGIN PLOT /ALICE_2019_I1716440/d[06,07,08,09,10,11,12,13]
XLabel=$p_\perp$ [GeV]
YLabel=Ratio
LogY=0
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d0[1,2,3,4,5]
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2019_I1716440/d01-x01-y01
Title=Cross section for prompt $D^0$ mesons at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d02-x01-y01
Title=Cross section for prompt $D^+$ mesons at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d03-x01-y01
Title=Cross section for prompt $D^{*\pm}$ mesons at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d04-x01-y01
Title=Cross section for prompt $D^+_s$ mesons at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d05-x01-y01
Title=Cross section for inclusive $D^0$ mesons at 5.02 TeV ($|y<0.5|$)
END PLOT

BEGIN PLOT /ALICE_2019_I1716440/d06-x01-y01
Title=Ratio of prompt $D^+/D^0$ production at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d07-x01-y01
Title=Ratio of prompt $D^{*+}/D^0$ production at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d08-x01-y01
Title=Ratio of prompt $D^+_s/D^0$ production at 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d09-x01-y01
Title=Ratio of prompt $D^+_s/D^+$ production at 5.02 TeV ($|y<0.5|$)
END PLOT

BEGIN PLOT /ALICE_2019_I1716440/d10-x01-y01
Title=Ratio of prompt $D^+/D^0$ production at 7 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d11-x01-y01
Title=Ratio of prompt $D^{*+}/D^0$ production at 7 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d12-x01-y01
Title=Ratio of prompt $D^+_s/D^0$ production at 7 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d13-x01-y01
Title=Ratio of prompt $D^+_s/D^+$ production at 7 TeV ($|y<0.5|$)
END PLOT

BEGIN PLOT /ALICE_2019_I1716440/d14-x01-y01
Title=Ratio of prompt $D^0$ production 7 TeV to 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d15-x01-y01
Title=Ratio of prompt $D^+$ production 7 TeV to 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d16-x01-y01
Title=Ratio of prompt $D^{*+}$ production 7 TeV to 5.02 TeV ($|y<0.5|$)
END PLOT
BEGIN PLOT /ALICE_2019_I1716440/d17-x01-y01
Title=Ratio of prompt $D^+_s$ production 7 TeV to 5.02 TeV ($|y<0.5|$)
END PLOT

BEGIN PLOT /ALICE_2019_I1716440/d18-x01-y01
Title=Cross section for prompt charm mesons at 5.02 TeV ($|y<0.5|$)
YLabel=$\sigma$ [$\mu$b]
XCustomMajorTicks=1. $D^0$	2. $D^+$	3. $D^{*\pm}$	4. $D^+_s$
XLabel=
END PLOT
