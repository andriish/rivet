Name: ALICE_2021_I1946131
Year: 2021
Summary: Prompt charm-meson production at 5 TeV
Experiment: ALICE
Collider: LHC
InspireID: 1946131
Status: VALIDATED
Reentrant: true
Authors:
 - Yonne Lourens <yonne_14@live.nl>
References:
 - 'JHEP. 01 (2022) 174'
 - 'DOI:10.1007/JHEP01(2022)174'
 - 'arXiv:2110.09420'
 - 'ALICE-6813'
RunInfo: Minimum bias events
Beams: [[p+,p+],[Pb,Pb]]
Energies: [5020,1044160]
Luminosity_fb: [20e-6, 186e-9]
Description:
  'The production of prompt $D^0$, $D^+$, and $D^{*+}$ mesons was measured at midrapidity ($|y| < $0.5) in Pb-Pb collisions at
  the centre-of-mass energy per nucleon-nucleon pair $\sqrt{s}_\text{NN} = 5.0$ TeV with the ALICE detector at the LHC. The $D$ mesons
  were reconstructed via their hadronic decay channels and their production yields were measured in central (0-10%) and semicentral
  (30-50%) collisions. The measurement was performed up to a transverse momentum ($p_\text{T}$) of 36 or 50 GeV/c depending on the
  $D$ meson species and the centrality interval. For the first time in Pb-Pb collisions at the LHC, the yield of $D^0$ mesons was
  measured down to $p_\text{T} =$ 0, which allowed a model-independent determination of the $p_\text{T}$-integrated yield per unit
  of rapidity ($\text{d}N/\text{d}y$). A maximum suppression by a factor 5 and 2.5 was observed with the nuclear modification factor
  ($R_\text{AA}$) of prompt $D$ mesons at $p_\text{T} =$ 6-8 GeV/c for the 0-10% and 30-50% centrality classes, respectively.
  The $D$-meson $R_\text{AA}$ is compared with that of charged pions, charged hadrons, and $J/\psi$ mesons as well as with
  theoretical predictions. The analysis of the agreement between the measured $R_\text{AA}$, elliptic ($v_2$) and triangular
  ($v_3$) flow, and the model predictions allowed us to constrain the charm spatial diffusion coefficient $D_\text{s}$.
  Furthermore the comparison of $R_\text{AA}$ and $v_2$ with different implementations of the same models provides an
  important insight into the role of radiative energy loss as well as charm quark recombination in the hadronisation mechanisms.'
Keywords: [PbPb collisions, prompt charm production]
BibKey: ALICE:2021rxa
BibTeX: '@article{ALICE:2021rxa,
    author = "Acharya, Shreyasi and others",
    collaboration = "ALICE",
    title = "{Prompt D$^{0}$, D$^{+}$, and D$^{*+}$ production in Pb\textendash{}Pb collisions at $ \sqrt{s_{\mathrm{NN}}} $ = 5.02 TeV}",
    eprint = "2110.09420",
    archivePrefix = "arXiv",
    primaryClass = "nucl-ex",
    reportNumber = "CERN-EP-2021-213",
    doi = "10.1007/JHEP01(2022)174",
    journal = "JHEP",
    volume = "01",
    pages = "174",
    year = "2022"
}'
