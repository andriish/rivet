Name: SND_2022_I2102082
Year: 2022
Summary: Cross section for $e^+e^-\to n\bar{n}$ between threshold and 2 GeV
Experiment: SND
Collider: VEPP-2M
InspireID: 2102082
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Eur.Phys.J.C 82 (2022) 8, 761
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies:  [1.8842, 1.8896, 1.9002, 1.9010, 1.9020, 1.9120, 1.9216,
            1.9254, 1.9428, 1.9460, 1.9642, 1.9764, 1.9828, 2.0068]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to n\bar{n}$ at energies between threshold and 2 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: SND:2022wdb
BibTeX: '@article{SND:2022wdb,
    author = "Achasov, M. N. and others",
    collaboration = "SND",
    title = "{Experimental study of the $e^+e^-\rightarrow n{\bar{n}}$ process at the VEPP-2000 $e^+e^-$ collider with the SND detector}",
    eprint = "2206.13047",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1140/epjc/s10052-022-10696-0",
    journal = "Eur. Phys. J. C",
    volume = "82",
    number = "8",
    pages = "761",
    year = "2022"
}
'
