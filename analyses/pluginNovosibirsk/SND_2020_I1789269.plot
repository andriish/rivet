BEGIN PLOT /SND_2020_I1789269/d01-x01-y01
Title=$\sigma(e^+e^-\to \pi^+\pi^-)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-)$/nb
ConnectGaps=1
END PLOT
