BEGIN PLOT /CMD3_2023_I2634277/d01
Title=Pion form factor
YLabel=$|F_\pi(\sqrt{s})|^2$
XLabel=$\sqrt{s}$ [MeV]
ConnectGaps=1
END PLOT
