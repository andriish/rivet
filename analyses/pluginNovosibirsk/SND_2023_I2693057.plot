BEGIN PLOT /SND_2023_I2693057/d01-x01-y01
Title=Cross section for $e^+e^-\to\omega\pi^0\to\pi^+\pi^-\pi^0\pi^0$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma(e^+e^-\to \omega\pi^0\to\pi^+\pi^-\pi^0\pi^0$ [nb]
LogY=1
ConnectGaps=1
END PLOT
