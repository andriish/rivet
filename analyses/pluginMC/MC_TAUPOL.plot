# BEGIN PLOT /MC_TAUPOL/h_m_tau
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_\tau$ [1/GeV]
XLabel=$m_\tau$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_tautau
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_{\tau\tau}$ [1/GeV]
XLabel=$m_{\tau\tau}$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_B
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m$ [1/GeV]
XLabel=$m (V)$ [GeV]
XLog=1
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_B_pT
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} p_\perp$ [1/GeV]
XLabel=$p_\perp(V)$ [GeV]
XLog=1
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_tau_vis
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_\tau^\mathrm{vis}$ [1/GeV]
XLabel=$m_\tau^\mathrm{vis}$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_tautau_vis
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_{\tau\tau}^\mathrm{vis}$ [1/GeV]
XLabel=$m_{\tau\tau}^\mathrm{vis}$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_pipi_vis
Title=near resonance
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_{\pi\pi}^\mathrm{vis}$ [1/GeV]
XLabel=$m_{\pi\pi}^\mathrm{vis}$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_pipi_vis_lm
Title=below resonance
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_{\pi\pi}^\mathrm{vis}$ [1/GeV]
XLabel=$m_{\pi\pi}^\mathrm{vis}$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_m_pipi_vis_hm
Title=above resonance
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} m_{\pi\pi}^\mathrm{vis}$ [1/GeV]
XLabel=$m_{\pi\pi}^\mathrm{vis}$ [GeV]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acoplanarity_plus
Title=$\alpha(\pi) > \frac{\pi}{4}$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acop_pt40
Title=$\alpha(\pi) > \frac{\pi}{4}$ and $p_\mathrm{T}(V)>40$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acop_pt100
Title=$\alpha(\pi) > \frac{\pi}{4}$ and $p_\mathrm{T}(V)>100$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acop_pt200
Title=$\alpha(\pi) > \frac{\pi}{4}$ and $p_\mathrm{T}(V)>200$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acop_pt1000
Title=$\alpha(\pi) > \frac{\pi}{4}$ and $p_\mathrm{T}(V)>1000$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acoplanarity_minus
Title=$\alpha(\pi) < \frac{\pi}{4}$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acom_pt40
Title=$\alpha(\pi) < \frac{\pi}{4}$ and $p_\mathrm{T}(V)>40$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acom_pt100
Title=$\alpha(\pi) < \frac{\pi}{4}$ and $p_\mathrm{T}(V)>100$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acom_pt200
Title=$\alpha(\pi) < \frac{\pi}{4}$ and $p_\mathrm{T}(V)>200$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_acom_pt1000
Title=$\alpha(\pi) < \frac{\pi}{4}$ and $p_\mathrm{T}(V)>1000$ GeV
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \phi^\ast$ [1/GeV]
XLabel=$\phi^\ast$ [rad]
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_2B_xlep
Title=decay-$\ell$ momentum fraction with respect to parent $\tau$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\ell) / p(\tau))$
XLabel=$p(\ell) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_2B_xel
Title=decay-$e$ momentum fraction with respect to parent $\tau$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(e) / p(\tau))$
XLabel=$p(e) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_2B_xmu
Title=decay-$\mu$ momentum fraction with respect to parent $\tau$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\mu) / p(\tau))$
XLabel=$p(\mu) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_xel_B
Title=decay-$e$ momentum fraction with respect to parent boson
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(e) / p(V))$
XLabel=$p(e) / p(V)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_xmu_B
Title=decay-$\mu$ momentum fraction with respect to parent boson
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\mu) / p(V))$
XLabel=$p(\mu) / p(V)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_1B_xpi
Title=decay-$\pi$ momentum fraction with respect to parent $\tau$ (near resonance)
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\pi) / p(\tau))$
XLabel=$p(\pi) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_1B_xpi_lm
Title=decay-$\pi$ momentum fraction with respect to parent $\tau$ (below resonance)
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\pi) / p(\tau))$
XLabel=$p(\pi) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_1B_xpi_hm
Title=decay-$\pi$ momentum fraction with respect to parent $\tau$ (above resonance)
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\pi) / p(\tau))$
XLabel=$p(\pi) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_xpi_B
Title=decay-$\pi$ momentum fraction with respect to parent boson
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\pi) / p(V))$
XLabel=$p(\pi) / p(V)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_2B_xrho
Title=decay-$\rho^\pm$ momentum fraction with respect to parent $\tau$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} (p(\rho^\pm) / p(\tau))$
XLabel=$p(\rho^\pm) / p(\tau)$
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_t_pi
Title=decay angle of the $\pi$ with respect to parent $\tau$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \cos\alpha(\pi,\tau)$
XLabel=$\cos\alpha(\pi,\tau)$ [rad]
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_t_rho
Title=decay angle of the $\rho^\pm$ with respect to parent $\tau$
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \cos\alpha(\rho^\pm,\tau)$
XLabel=$\cos\alpha(\rho^\pm,\tau)$ [rad]
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_pipu_theta
Title=angle between the pion pair in the rest frame
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} \cos\theta(\pi,\pi)$
XLabel=$\cos\theta(\pi,\pi)$ [rad]
LogY=0
# END PLOT

# BEGIN PLOT /MC_TAUPOL/h_n_taus
Title=$\tau$ multiplicity
YLabel=$1/\sigma$ $\mathrm{d}\sigma / \mathrm{d} N_\tau$
XLabel=$N_\tau$
LogY=0
# END PLOT
