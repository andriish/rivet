# BEGIN PLOT /MC_MET/met_incl
Title=Vector missing $E_\mathrm{T}^\mathrm{miss}$ in full acceptance
XLabel=Vector missing $E_\mathrm{T}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\sum E_\mathrm{T}^\mathrm{miss}$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_MET/set_incl
Title=Scalar $\sum E_\mathrm{T}^\mathrm{miss}$ in full acceptance
XLabel=Scalar $\sum E_\mathrm{T}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\sum E_\mathrm{T}^\mathrm{miss}$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_MET/met_calo
Title=Vector missing $E_\mathrm{T}^{miss}$ $|\eta| < 5$ acceptance
XLabel=Vector missing $E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} E_\mathrm{T}^{miss}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/set_calo
Title=Scalar $\sum E_\mathrm{T}$ $|\eta| < 5$ acceptance
XLabel=Scalar $\sum E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\sum E_\mathrm{T}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/pT_inv
Title=$p_\mathrm{T}$ of vector sum of all invisibles
XLabel=$p_\mathrm{T}^\mathrm{inv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^\mathrm{inv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/mass_inv
Title=Mass $m^\mathrm{inv}$ of vector sum of all invisibles
XLabel=$m^\mathrm{inv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^\mathrm{inv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/rap_inv
Title=Rapidity $y^\mathrm{inv}$ of vector sum of all invisibles
XLabel=$y^\mathrm{inv}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y^\mathrm{inv}$ [pb]
# END PLOT

# BEGIN PLOT /MC_MET/pT_promptinv
Title=$p_\mathrm{T}$ of vector sum of all prompt invisibles
XLabel=$p_\mathrm{T}^\mathrm{pinv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^\mathrm{pinv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/mass_promptinv
Title=Mass $m^\mathrm{pinv}$ of vector sum of all prompt invisibles
XLabel=$m^\mathrm{pinv}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^\mathrm{pinv}$ [pb GeV$^{-1}$]
LogX=1
# END PLOT

# BEGIN PLOT /MC_MET/rap_promptinv
Title=Rapidity $y^\mathrm{pinv}$ of vector sum of all prompt invisibles
XLabel=$y^\mathrm{pinv}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y^\mathrm{pinv}$ [pb]
# END PLOT
