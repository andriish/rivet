BEGIN PLOT /MARKII_1986_I220003/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-/K^+K^-$ with $|\cos\theta|<0.3$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-/K^+K^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /MARKII_1986_I220003/d01-x01-y02
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-/K^+K^-$ with $0.3<|\cos\theta|<0.5$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-/K^+K^-)$ [nb]
LogY=1
END PLOT
