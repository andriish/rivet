BEGIN PLOT /HRS_1987_I246162/d01-x01-y01
Title=$\Sigma^{*\pm}$ multiplicity at 29 GeV
XLabel=$x_E$
YLabel=$\langle N_{\Sigma^{*\pm}}\rangle$
LogY=0
END PLOT
BEGIN PLOT /HRS_1987_I246162/d01-x01-y02
Title=$\Sigma^{*\pm}$ multiplicity at 29 GeV
XLabel=$x_E$
YLabel=$\langle N_{\Sigma^{*\pm}}\rangle$
LogY=0
END PLOT

BEGIN PLOT /HRS_1987_I246162/d02-x01-y01
Title=$\Xi^-$ multiplicity at 29 GeV
XLabel=$x_E$
YLabel=$\langle N_{\Xi^-}\rangle$
LogY=0
END PLOT
BEGIN PLOT /HRS_1987_I246162/d02-x01-y02
Title=$\Xi^-$ multiplicity at 29 GeV
XLabel=$x_E$
YLabel=$\langle N_{\Xi^-}\rangle$
LogY=0
END PLOT

BEGIN PLOT /HRS_1987_I246162/d03-x01-y01
Title=Ratio of $\Sigma^{*\pm}$ to $\Lambda^0$ multiplicity at 29 GeV
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle N_{\Sigma^{*\pm}}\rangle/\langle N_{\Lambda^0}\rangle$
LogY=0
END PLOT
BEGIN PLOT /HRS_1987_I246162/d03-x01-y02
Title=Ratio of $\Xi^-$ to $\Lambda^0$ multiplicity at 29 GeV
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\langle N_{\Xi^-}\rangle/\langle N_{\Lambda^0}\rangle$
LogY=0
END PLOT

BEGIN PLOT /HRS_1987_I246162/d04-x01-y01
Title=$\Sigma^{*\pm}$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$s/\beta \text{d}\sigma/\text{d}x_E$ [$\text{nb}\text{GeV}^2$]
END PLOT

BEGIN PLOT /HRS_1987_I246162/d04-x01-y02
Title=$\Xi^-$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$s/\beta \text{d}\sigma/\text{d}x_E$ [$\text{nb}\text{GeV}^2$]
END PLOT
