Name: MARKI_1976_I108144
Year: 1976
Summary: Measurement of $R$ between 3.888 and 4.586 GeV
Experiment: MARKI
Collider: Spear
InspireID: 108144
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 36 (1976) 700, 1976 
RunInfo: e+ e- to hadrons and e+ e- to mu+ mu- (for normalization)
NeedCrossSection: no
Beams: [e-, e+]
Energies:  [3.899, 3.919, 3.9386, 3.9586, 3.9782, 3.9974, 4.0178, 4.0286, 4.037, 4.057, 4.0762,
            4.3318, 4.3508, 4.371, 4.3904, 4.4042, 4.414, 4.4196, 4.4292, 4.4494, 4.4686, 4.5126,
            3.888, 3.908, 3.926, 3.946, 3.966, 3.986, 4.006, 4.024, 4.038, 4.046, 4.066, 4.086,
            4.106, 4.112, 4.126, 4.136, 4.146, 4.164, 4.186, 4.206, 4.226, 4.244, 4.264, 4.284,
            4.302, 4.326, 4.346, 4.366, 4.384, 4.406, 4.426, 4.438, 4.444, 4.464, 4.484, 4.536, 4.586]
Description:
  'Measurement of $R$ in $e^+e^-$ collisions by MARKI for energies between 3.888 and 4.586 GeV.
   The individual hadronic and muonic cross sections are also outputted to the yoda file
   so that ratio $R$ can be recalcuated if runs are combined.'
BibKey: Siegrist:1976br
BibTeX: '@article{Siegrist:1976br,
      author         = "Siegrist, J. and others",
      title          = "{Observation of a Resonance at 4.4-GeV and Additional
                        Structure Near 4.1-GeV in e+ e- Annihilation}",
      journal        = "Phys. Rev. Lett.",
      volume         = "36",
      year           = "1976",
      pages          = "700",
      doi            = "10.1103/PhysRevLett.36.700",
      reportNumber   = "SLAC-PUB-1717, LBL-4804",
      SLACcitation   = "%%CITATION = PRLTA,36,700;%%"
}'
