BEGIN PLOT /HRS_1986_I17781/d01-x01-y01
Title=Scaled energy for $\Lambda^0,\bar{\Lambda}^0$ at 29 GeV
XLabel=$x_E$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1986_I17781/d02-x01-y01
Title=Rapidity w.r.t to thrust axis for $\Lambda^0,\bar{\Lambda}^0$ at 29 GeV
XLabel=$y$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}y$
END PLOT
BEGIN PLOT /HRS_1986_I17781/d03-x01-y01
Title=$\Lambda^0,\bar{\Lambda}^0$ Multiplicity at 29 GeV
XLabel=$\sqrt{s}$ [GeV]
YLabel=$N_{\Lambda}$
LogY=0
END PLOT
