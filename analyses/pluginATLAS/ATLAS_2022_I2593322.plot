# BEGIN PLOT /ATLAS_2022_I2593322/d*
XTwosidedTicks=1
YTwosidedTicks=1
LogY=0
LeftMargin=1.5
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2593322/d02-x01-y01
XLabel=$E^{\gamma1}_{\mathrm{T}}$ [GeV]
YLabel=$d\sigma/dE^{\gamma1}_{\mathrm{T}}$ [fb $\mathrm{GeV^{-1}}$]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2593322/d03-x01-y01
XLabel=$E^{\gamma2}_{\mathrm{T}}$ [GeV]
YLabel=$d\sigma/dE^{\gamma2}_{\mathrm{T}}$ [fb $\mathrm{GeV^{-1}}$]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2593322/d04-x01-y01
XLabel=$p^{\ell\ell}_{\mathrm{T}}$ [GeV]
YLabel=$d\sigma/dp^{\ell\ell}_{\mathrm{T}}$ [fb $\mathrm{GeV^{-1}}$]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2593322/d05-x01-y01
XLabel=$p^{\ell\ell\gamma\gamma}_{\mathrm{T}}$ [GeV]
YLabel=$d\sigma/dp^{\ell\ell\gamma\gamma}_{\mathrm{T}}$ [fb $\mathrm{GeV^{-1}}$]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2593322/d06-x01-y01
XLabel=$m_{\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dm_{\gamma\gamma}$ [fb $\mathrm{GeV^{-1}}$]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2593322/d07-x01-y01
XLabel=$m_{\ell\ell\gamma\gamma}$ [GeV]
YLabel=$d\sigma/dm_{\ell\ell\gamma\gamma}$ [fb $\mathrm{GeV^{-1}}$]
LogY=0
# END PLOT

