# BEGIN PLOT /ATLAS_2010_I871366/d01-x01-y01
Title=Inclusive jet $p_T$ spectrum for $|y|<0.3$.  anti-KT, $R=0.4$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d02-x01-y01
Title=Inclusive jet $p_T$ spectrum for $0.3<|y|<0.8$.  anti-KT, $R=0.4$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d03-x01-y01
Title=Inclusive jet $p_T$ spectrum for $0.8<|y|<1.2$.  anti-KT, $R=0.4$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d04-x01-y01
Title=Inclusive jet $p_T$ spectrum for $1.2<|y|<2.1$.  anti-KT, $R=0.4$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d05-x01-y01
Title=Inclusive jet $p_T$ spectrum for $2.1<|y|<2.8$.  anti-KT, $R=0.4$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d06-x01-y01
Title=Inclusive jet $p_T$ spectrum for $|y|<0.3$.  anti-KT, $R=0.6$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d07-x01-y01
Title=Inclusive jet $p_T$ spectrum for $0.3<|y|<0.8$.  anti-KT, $R=0.6$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d08-x01-y01
Title=Inclusive jet $p_T$ spectrum for $0.8<|y|<1.2$.  anti-KT, $R=0.6$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d09-x01-y01
Title=Inclusive jet $p_T$ spectrum for $1.2<|y|<2.1$.  anti-KT, $R=0.6$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d10-x01-y01
Title=Inclusive jet $p_T$ spectrum for $2.1<|y|<2.8$.  anti-KT, $R=0.6$.
XLabel=$p_{T}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}p_{T}\,\mathrm{d}y$ [pb/GeV]
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d11-x01-y01
Title=Dijet mass spectrum for $|y_\mathrm{max}|<0.3$.  anti-KT, $R=0.4$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d12-x01-y01
Title=Dijet mass spectrum for $0.3<|y_\mathrm{max}|<0.8$.  anti-KT, $R=0.4$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d13-x01-y01
Title=Dijet mass spectrum for $0.8<|y_\mathrm{max}|<1.2$.  anti-KT, $R=0.4$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d14-x01-y01
Title=Dijet mass spectrum for $1.2<|y_\mathrm{max}|<2.1$.  anti-KT, $R=0.4$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d15-x01-y01
Title=Dijet mass spectrum for $2.1<|y_\mathrm{max}|<2.8$.  anti-KT, $R=0.4$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d16-x01-y01
Title=Dijet mass spectrum for $|y_\mathrm{max}|<0.3$.  anti-KT, $R=0.6$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d17-x01-y01
Title=Dijet mass spectrum for $0.3<|y_\mathrm{max}|<0.8$.  anti-KT, $R=0.6$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d18-x01-y01
Title=Dijet mass spectrum for $0.8<|y_\mathrm{max}|<1.2$.  anti-KT, $R=0.6$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d19-x01-y01
Title=Dijet mass spectrum for $1.2<|y_\mathrm{max}|<2.1$.  anti-KT, $R=0.6$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d20-x01-y01
Title=Dijet mass spectrum for $2.1<|y_\mathrm{max}|<2.8$.  anti-KT, $R=0.6$.
XLabel=$m_{12}$ [GeV]
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}m\,\mathrm{d}|y_\mathrm{max}|$ [pb/GeV]
LogX=1
LogY=1
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d21-x01-y01
Title=Dijet $\chi$ for $340~GeV<m_{12}<520~GeV$.  anti-KT, $R=0.4$.
XLabel=$\chi$
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}\chi\,\mathrm{d}m_{12}$ [pb/GeV]
LogX=1
LogY=0
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d22-x01-y01
Title=Dijet $\chi$ for $520~GeV<m_{12}<800~GeV$.  anti-KT, $R=0.4$.
XLabel=$\chi$
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}\chi\,\mathrm{d}m_{12}$ [pb/GeV]
LogX=1
LogY=0
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d23-x01-y01
Title=Dijet $\chi$ for $800~GeV<m_{12}<1200~GeV$.  anti-KT, $R=0.4$.
XLabel=$\chi$
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}\chi\,\mathrm{d}m_{12}$ [pb/GeV]
LogX=1
LogY=0
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d24-x01-y01
Title=Dijet $\chi$ for $340~GeV<m_{12}<520~GeV$.  anti-KT, $R=0.6$.
XLabel=$\chi$
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}\chi\,\mathrm{d}m_{12}$ [pb/GeV]
LogX=1
LogY=0
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d25-x01-y01
Title=Dijet $\chi$ for $520~GeV<m_{12}<800~GeV$.  anti-KT, $R=0.6$.
XLabel=$\chi$
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}\chi\,\mathrm{d}m_{12}$ [pb/GeV]
LogX=1
LogY=0
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_2010_I871366/d26-x01-y01
Title=Dijet $\chi$ for $800~GeV<m_{12}<1200~GeV$.  anti-KT, $R=0.6$.
XLabel=$\chi$
YLabel=$\mathrm{d}^{2}\sigma/\mathrm{d}\chi\,\mathrm{d}m_{12}$ [pb/GeV]
LogX=1
LogY=0
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT
