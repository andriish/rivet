BEGIN PLOT /ATLAS_2016_I1408878/d01-x01-y01
Title=Cross section for $D^{*\pm}$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma$ [$\mu$b]
END PLOT
BEGIN PLOT /ATLAS_2016_I1408878/d01-x01-y02
Title=Cross section for $D^{\pm}$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma$ [$\mu$b]
END PLOT
BEGIN PLOT /ATLAS_2016_I1408878/d01-x01-y03
Title=Cross section for $D_s^{\pm}$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT

BEGIN PLOT /ATLAS_2016_I1408878/d02-x01-y01
Title=Cross section for $D^{*\pm}$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /ATLAS_2016_I1408878/d02-x01-y02
Title=Cross section for $D^{\pm}$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT

BEGIN PLOT /ATLAS_2016_I1408878/d03-x01-y01
Title=Cross section for $D^{*\pm}$ production $3.5<p_\perp<20$\,GeV
XLabel=$\eta$
YLabel=$\text{d}\sigma/\text{d}\eta$ [$\mu$b]
LogY=0
END PLOT
BEGIN PLOT /ATLAS_2016_I1408878/d03-x01-y02
Title=Cross section for $D^{\pm}$ production $3.5<p_\perp<20$\,GeV
XLabel=$\eta$
YLabel=$\text{d}\sigma/\text{d}\eta$ [$\mu$b]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2016_I1408878/d04-x01-y01
Title=Cross section for $D^{*\pm}$ production $20<p_\perp<100$\,GeV
XLabel=$\eta$
YLabel=$\text{d}\sigma/\text{d}\eta$ [nb]
LogY=0
END PLOT
BEGIN PLOT /ATLAS_2016_I1408878/d04-x01-y02
Title=Cross section for $D^{\pm}$ production $20<p_\perp<100$\,GeV
XLabel=$\eta$
YLabel=$\text{d}\sigma/\text{d}\eta$ [nb]
LogY=0
END PLOT
