BEGIN PLOT /CMS_2019_I1672011/d03
XLabel=$p_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2019_I1672011/d03-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$  ($0<y<0.9$) 
END PLOT
BEGIN PLOT /CMS_2019_I1672011/d03-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$  ($0.9<y<1.5$) 
END PLOT
BEGIN PLOT /CMS_2019_I1672011/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$  ($1.5<y<1.93$) 
END PLOT
BEGIN PLOT /CMS_2019_I1672011/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$  ($1.93<y<2.4$) 
END PLOT
