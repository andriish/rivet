BEGIN PLOT /CMS_2011_I896211/d01-x01-y01
Title=Cross section for $B^0$ production ($|y|<2.4$, $p_\perp>5$\,GeV)
YLabel=$\sigma$   [$\mu$b]
LogY=0
END PLOT
BEGIN PLOT /CMS_2011_I896211/d02-x01-y01
Title=Cross section for $B^0$ production ($|y|<2.4$)
YLabel=$\text{d}\sigma/\text{d}p_\perp$   [$\mu$b/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /CMS_2011_I896211/d03-x01-y01
Title=Cross section for $B^0$ production ($p_\perp>5$\,GeV)
YLabel=$\text{d}\sigma/\text{d}y$
XLabel=$y$
LogY=0
END PLOT
