BEGIN PLOT /CMS_2021_I1963239/d07-x01-y01
Title=CMS, pp 2.76 TeV, inclusive cross section, Figure 2
XLabel=$\Delta y$
YLabel=$\mathrm{d} \sigma^\mathrm{inc} / \mathrm{d} \Delta y$ [pb]
FullRange=1
LogY=1
END PLOT

BEGIN PLOT /CMS_2021_I1963239/d08-x01-y01
Title=CMS, pp 2.76 TeV, Mueller-Navelet cross section, Figure 3
XLabel=$\Delta y$
YLabel=$\mathrm{d} \sigma^\mathrm{MN} / \mathrm{d} \Delta y$ [pb]
FullRange=1
LogY=1
END PLOT

BEGIN PLOT /CMS_2021_I1963239/d09-x01-y01
Title=CMS, pp 2.76 TeV, Ratio $R^\mathrm{incl}$, Figure 4
XLabel=$\Delta y$
YLabel=$R^\mathrm{incl}$
FullRange=1
LogY=0
END PLOT

BEGIN PLOT /CMS_2021_I1963239/d10-x01-y01
Title=CMS, pp 2.76 TeV, Ratio $R^\mathrm{incl}_\mathrm{veto}$, Figure 5
XLabel=$\Delta y$
YLabel=$R^\mathrm{incl}_\mathrm{veto}$
FullRange=1
LogY=0
END PLOT

BEGIN PLOT /CMS_2021_I1963239/d11-x01-y01
Title=CMS, pp 2.76 TeV, Ratio $R^\mathrm{MN}$, Figure 6
XLabel=$\Delta y$
YLabel=$R^\mathrm{MN}$
FullRange=1
LogY=0
END PLOT

BEGIN PLOT /CMS_2021_I1963239/d12-x01-y01
Title=CMS, pp 2.76 TeV, Ratio $R^\mathrm{MN}_\mathrm{veto}$, Figure 7
XLabel=$\Delta y$
YLabel=$R^\mathrm{MN}_\mathrm{veto}$
FullRange=1
LogY=0
END PLOT





