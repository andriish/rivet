Name: CMS_2012_I944755
Year: 2012
Summary: J/$\psi$ and $\psi(2S)$ production at 7 TeV
Experiment: CMS
Collider: LHC
InspireID: 944755
Status: VALIDATED
Reentrant: True
Authors:
 - Peter Richardson <peter.richardson@durhama.ac.uk>
References:
 - JHEP 02 (2012) 011
 - arXiv:1111.1557 [hep-ex]
 - CMS-BPH-10-014
RunInfo: J/psi and psi(2s) production in pp
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of prompt and non-prompt $J/\psi$ and $\psi(2S)$ at 7 TeV by the CMS collaboration.
  The transverse momentum spectra are measured in a number of rapidity intervals, together with the ratio of the production of the $\psi(2S)$ to the $J/\psi$.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CMS:2011rxs
BibTeX: '@article{CMS:2011rxs,
    author = "Chatrchyan, Serguei and others",
    collaboration = "CMS",
    title = "{$J/\psi$ and $\psi_{2S}$ production in $pp$ collisions at $\sqrt{s}=7$ TeV}",
    eprint = "1111.1557",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CMS-BPH-10-014, CERN-PH-EP-2011-177",
    doi = "10.1007/JHEP02(2012)011",
    journal = "JHEP",
    volume = "02",
    pages = "011",
    year = "2012"
}'
