# BEGIN PLOT /CMS_2021_I1972986/d*
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d01-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $0.0 < |y| < 0.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d02-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $0.5 < |y| < 1.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d03-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $1.0 < |y| < 1.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d04-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK4 jets, $1.5 < |y| < 2.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT



# BEGIN PLOT /CMS_2021_I1972986/d21-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $0.0 < |y| < 0.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d22-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $0.5 < |y| < 1.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d23-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $1.0 < |y| < 1.5$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

# BEGIN PLOT /CMS_2021_I1972986/d24-x01-y01
Title=CMS, $\sqrt(s)$ = 13 TeV, Inclusive AK7 jets, $1.5 < |y| < 2.0$
XLabel=$p_{T}$ [GeV]
YLabel=d$^2\sigma$/d$p_{T}$d$y$ [pb/GeV]
FullRange=1
LogY=1
LogX=1
RatioPlotYMax=1.4
RatioPlotYMin=0.6
# END PLOT

