BEGIN PLOT /CMS_2013_I1244128
YMin=-1.
YMax=1.
LogY=0
XLabel=$p_\perp$ [GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1244128/d01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d04
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d05
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d06
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d07
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d08
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d09
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d10
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d11
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d12
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d13
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d14
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d15
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d16
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d17
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d18
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d19
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d20
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d21
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d22
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d23
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for J/$\psi$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d24
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for J/$\psi$ ($0.6<|y|<1.2$)
END PLOT

BEGIN PLOT /CMS_2013_I1244128/d25
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d26
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d27
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d28
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d29
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d30
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d31
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d32
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d33
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d34
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d35
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d36
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d37
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d38
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d39
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d40
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d41
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d42
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d43
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d44
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d45
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d46
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d47
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d48
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d49
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d50
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d51
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d52
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d53
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d54
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d55
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d56
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d57
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d58
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\psi(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d59
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\psi(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1244128/d60
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\psi(2S)$ ($1.2<|y|<1.5$)
END PLOT
