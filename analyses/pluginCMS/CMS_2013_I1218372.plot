# BEGIN PLOT /CMS_2013_I1218372/d0
XLabel=Leading charged jet $p_\perp$ [GeV]
YLabel=$(\mathrm{d}E^\mathrm{hard} / \mathrm{d}\eta) / (\mathrm{d} E^\mathrm{incl} / \mathrm{d}\eta)$
LogY=0
# END PLOT


# BEGIN PLOT /CMS_2013_I1218372/d01-x01-y01
Title=Ratio of energy deposited in $-6.6 < \eta < -5.2$ for $\sqrt{s}=0.9$ TeV
# END PLOT

# BEGIN PLOT /CMS_2013_I1218372/d02-x01-y01
Title=Ratio of energy deposited in $-6.6 < \eta < -5.2$ for $\sqrt{s}=2.76$ TeV
# END PLOT

# BEGIN PLOT /CMS_2013_I1218372/d03-x01-y01
Title=Ratio of energy deposited in $-6.6 < \eta < -5.2$ for $\sqrt{s}=7$ TeV
# END PLOT
