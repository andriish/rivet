BEGIN PLOT /CMS_2013_I1225274/d01-x01-y01
Title=$\Upsilon$ cross section $|y|<2.4$, $p_\perp<50\,$ GeV (fiducal $\mu$ cuts)
YLabel=$\sigma(\Upsilon)\times\text{Br}(\Upsilon\to\mu^+\mu^-)$ [nb]
XLabel=State
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d01-x01-y02
Title=$\Upsilon$ cross section $|y|<2.4$, $p_\perp<50\,$ GeV ($\mu$ acceptance corr)
YLabel=$\sigma(\Upsilon)\times\text{Br}(\Upsilon\to\mu^+\mu^-)$ [nb]
XLabel=State
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d02-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d02-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $|y|<2.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d03-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d03-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $|y|<2.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d04-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d04-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $|y|<2.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d29-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $|y|<1.2$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d30-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $|y|<1.2$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d31-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $|y|<1.2$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d05-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $|y|<0.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d05-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $|y|<0.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d06-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $|y|<0.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d06-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $|y|<0.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d07-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $|y|<0.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d07-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $|y|<0.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d08-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $0.4<|y|<0.8$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d08-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $0.4<|y|<0.8$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d09-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $0.4<|y|<0.8$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d09-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $0.4<|y|<0.8$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d10-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $0.4<|y|<0.8$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d10-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $0.4<|y|<0.8$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d11-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $0.8<|y|<1.2$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d11-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $0.8<|y|<1.2$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d12-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $0.8<|y|<1.2$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d12-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $0.8<|y|<1.2$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d13-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $0.8<|y|<1.2$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d13-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $0.8<|y|<1.2$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d14-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $1.2<|y|<1.6$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d14-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $1.2<|y|<1.6$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d15-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $1.2<|y|<1.6$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d15-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $1.2<|y|<1.6$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d16-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $1.2<|y|<1.6$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d16-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $1.2<|y|<1.6$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d17-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $1.6<|y|<2.0$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d17-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $1.6<|y|<2.0$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d18-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $1.6<|y|<2.0$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d18-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $1.6<|y|<2.0$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d19-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $1.6<|y|<2.0$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d19-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $1.6<|y|<2.0$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d20-x01-y01
Title=$\Upsilon(1S)$ $p_\perp$ $2.0<|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d20-x01-y02
Title=$\Upsilon(1S)$ $p_\perp$ $2.0<|y|<2.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d21-x01-y01
Title=$\Upsilon(2S)$ $p_\perp$ $2.0<|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d21-x01-y02
Title=$\Upsilon(2S)$ $p_\perp$ $2.0<|y|<2.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d22-x01-y01
Title=$\Upsilon(3S)$ $p_\perp$ $2.0<|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d22-x01-y02
Title=$\Upsilon(3S)$ $p_\perp$ $2.0<|y|<2.4$ ($\mu$ acceptance corr)
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT


BEGIN PLOT /CMS_2013_I1225274/d23-x01-y01
Title=$\Upsilon(1S)$$y$ $p_\perp<50\,$GeV (fiducal $\mu$ cuts)
XLabel=$y_{\Upsilon(1S)}$
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d23-x01-y02
Title=$\Upsilon(1S)$$y$ $p_\perp<50\,$GeV ($\mu$ acceptance corr)
XLabel=$y_{\Upsilon(1S)}$
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d24-x01-y01
Title=$\Upsilon(2S)$$y$ $p_\perp<50\,$GeV (fiducal $\mu$ cuts)
XLabel=$y_{\Upsilon(2S)}$
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d24-x01-y02
Title=$\Upsilon(2S)$$y$ $p_\perp<50\,$GeV ($\mu$ acceptance corr)
XLabel=$y_{\Upsilon(2S)}$
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d25-x01-y01
Title=$\Upsilon(3S)$$y$ $p_\perp<50\,$GeV (fiducal $\mu$ cuts)
XLabel=$y_{\Upsilon(3S)}$
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d25-x01-y02
Title=$\Upsilon(3S)$$y$ $p_\perp<50\,$GeV ($\mu$ acceptance corr)
XLabel=$y_{\Upsilon(3S)}$
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d26-x01-y01
Title=$\Upsilon(3S)/\Upsilon(1S)$ $p_\perp$ $|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p_\perp$ [GeV]
YLabel=$R_{31}$
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d26-x01-y02
Title=$\Upsilon(3S)/\Upsilon(1S)$ $p_\perp$ ($\mu$ acceptance corr)
XLabel=$p_\perp$ [GeV]
YLabel=$R_{31}$
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d27-x01-y01
Title=$\Upsilon(2S)/\Upsilon(1S)$ $p_\perp$ $|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d27-x01-y02
Title=$\Upsilon(2S)/\Upsilon(1S)$ $p_\perp$ ($\mu$ acceptance corr)
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
END PLOT

BEGIN PLOT /CMS_2013_I1225274/d28-x01-y01
Title=$\Upsilon(3S)/\Upsilon(2S)$ $p_\perp$ $|y|<2.4$ (fiducal $\mu$ cuts)
XLabel=$p_\perp$ [GeV]
YLabel=$R_{32}$
END PLOT
BEGIN PLOT /CMS_2013_I1225274/d28-x01-y02
Title=$\Upsilon(3S)/\Upsilon(2S)$ $p_\perp$ ($\mu$ acceptance corr)
XLabel=$p_\perp$ [GeV]
YLabel=$R_{32}$
END PLOT
