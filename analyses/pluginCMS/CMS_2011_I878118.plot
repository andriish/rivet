BEGIN PLOT /CMS_2011_I878118/d02-x01-y01
Title=Prompt $J\/\psi$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{J\/\psi}_\perp$ [GeV]
YLabel=Br($J\/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2011_I878118/d03-x01-y01
Title=Prompt $J/\psi$ transverse momentum $1.2 < |y| < 1.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2011_I878118/d04-x01-y01
Title=Prompt $J/\psi$ transverse momentum $1.6 < |y| < 2.4$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2011_I878118/d05-x01-y01
Title=Non-prompt $J\/\psi$ fraction $0 < |y| < 1.2$
XLabel=$p^{J\/\psi}_\perp$ [GeV]
YLabel=Non-prompt $J\/\psi$ fraction
END PLOT
BEGIN PLOT /CMS_2011_I878118/d06-x01-y01
Title=Non-prompt $J\/\psi$ fraction $1.2 < |y| < 1.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Non-prompt $J\/\psi$ fraction
END PLOT
BEGIN PLOT /CMS_2011_I878118/d07-x01-y01
Title=Non-prompt $J\/\psi$ fraction $1.6 < |y| < 2.4$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Non-prompt $J\/\psi$ fraction
END PLOT

BEGIN PLOT /CMS_2011_I878118/d11-x01-y01
Title=Non-prompt $J\/\psi$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{J\/\psi}_\perp$ [GeV]
YLabel=Br($J\/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2011_I878118/d12-x01-y01
Title=Non-prompt $J/\psi$ transverse momentum $1.2 < |y| < 1.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2011_I878118/d13-x01-y01
Title=Non-prompt $J/\psi$ transverse momentum $1.6 < |y| < 2.4$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT