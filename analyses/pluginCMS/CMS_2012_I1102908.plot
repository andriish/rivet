# BEGIN PLOT /CMS_2012_I1102908/d01-x01-y01
Title=Inclusive to exclusive dijet production ratio
XLabel=$\Delta y$
YLabel=$\frac{\mathrm{d} \sigma^\mathrm{inc}} {\mathrm{d} y}  /  \frac{\mathrm{d} \sigma^\mathrm{exc}}{\mathrm{d} y}$
FullRange=1
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2012_I1102908/d02-x01-y01
Title=Mueller-Navelet to exclusive dijet production ratio
XLabel=$\Delta y$
YLabel=$\frac{\mathrm{d} \sigma^\mathrm{MN}}{\mathrm{d} y} / \frac{\mathrm{d} \sigma^\mathrm{exc}}{\mathrm{d} y}$
FullRange=1
LogY=0
# END PLOT
