BEGIN PLOT /H1_1996_I424463/d*
Title=Charged Particle Spectra H1
END PLOT

BEGIN PLOT /H1_1996_I424463/d01-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 5 < Q^2 < 50$ GeV$^2$ \\$ 0.0001 < x < 0.001 $
LegendTitle= $ <Q^2> = 18.3$ GeV$^2$ \\$ <x> =0.00114 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d02-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 5 < Q^2 < 10$ GeV$^2$ \\$ 0.0001 < x < 0.0002 $
LegendTitle= $  <Q^2> =7$ GeV$^2$ \\$ <x>=0.00016 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d03-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 6 < Q^2 < 10$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $ < Q^2>= 8.8$ GeV$^2$ \\$  < x> = 0.00029 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d04-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $ < Q^2>= 13.1 $ GeV$^2$ \\$  < x> = 0.00037 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d05-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0005 < x < 0.0008 $
LegendTitle= $ < Q^2 >=14$ GeV$^2$ \\$< x> 0.00064 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d06-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0008 < x < 0.0015 $
LegendTitle= $ < Q^2> = 14.3$ GeV$^2$ \\$ < x >= 0.0011 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d07-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0015 < x < 0.004 $
LegendTitle= $  < Q^2 > =15.3$ GeV$^2$ \\$ < x >= 0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d08-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0005 < x < 0.0014 $
LegendTitle= $ < Q^2 > = 28.6$ GeV$^2$ \\$  < x >= 0.00093 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d09-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0014 < x < 0.003 $
LegendTitle= $  < Q^2 >=31,6 $ GeV$^2$ \\$  < x >= 0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d10-x01-y01
Title=Charged Particle Spectra H1 $1.5 < \eta^* < 2.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.003 < x < 0.01 $
LegendTitle= $  < Q^2 >=34.7 $ GeV$^2$ \\$ < x >= 0.0044 $
END PLOT

BEGIN PLOT /H1_1996_I424463/d11-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 5 < Q^2 < 50$ GeV$^2$ \\$ 0.0001 < x < 0.001 $
LegendTitle= $  < Q^2 > 18.3 $ GeV$^2$ \\$  < x < 0.0014 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d12-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 5 < Q^2 < 10$ GeV$^2$ \\$ 0.0001 < x < 0.0002 $
LegendTitle= $  < Q^2 > =7 $ GeV$^2$ \\$  < x >= 0.00016 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d13-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 6 < Q^2 < 10$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $ < Q^2 > =8.8 $ GeV$^2$ \\$ < x >= 0.00029 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d14-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $  < Q^2 >=13.1$ GeV$^2$ \\$  < x>= 0.00037 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d15-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0005 < x < 0.0008 $
LegendTitle= $  < Q^2>14.0$ GeV$^2$ \\$  < x >= 0.00064 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d16-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0008 < x < 0.0015 $
LegendTitle= $  < Q^2> = 14.3$ GeV$^2$ \\$  < x >= 0.0011 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d17-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0015 < x < 0.004 $
LegendTitle= $ < Q^2 >=15.2$ GeV$^2$ \\$  < x >= 0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d18-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0005 < x < 0.0014 $
LegendTitle= $  < Q^2>=28.6$ GeV$^2$ \\$  < x > =0.00093 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d19-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0014 < x < 0.003 $
LegendTitle= $ < Q^2>=31.6$ GeV$^2$ \\$  < x >= 0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d20-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^*$ [GeV]
YLabel=$1/N dn/dp_T^*$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.003 < x < 0.01 $
LegendTitle= $  < Q^2>34.7 $ GeV$^2$ \\$  < x >= 0.0044 $
END PLOT

BEGIN PLOT /H1_1996_I424463/d21-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 5 < Q^2 < 50$ GeV$^2$ \\$ 0.0001 < x < 0.001 $
LegendTitle= $  < Q^2> 18.3$ GeV$^2$ \\$  < x > 0.00014 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d22-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 5 < Q^2 < 10$ GeV$^2$ \\$ 0.0001 < x < 0.0002 $
LegendTitle= $  < Q^2 >=7$ GeV$^2$ \\$  < x >=0.00016 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d23-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 6 < Q^2 < 10$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $  < Q^2 > = 8.8 $ GeV$^2$ \\$  < x >= 0.00029 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d24-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $  < Q^2 > 13.1$ GeV$^2$ \\$  < x >= 0.00037 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d25-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0005 < x < 0.0008 $
LegendTitle= $ < Q^2 >= 14.00$ GeV$^2$ \\$  < x >=0.00064 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d26-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0008 < x < 0.0015 $
LegendTitle= $  < Q^2>=14.3$ GeV$^2$ \\$  < x >= 0.0011 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d27-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0005 < x < 0.0014 $
LegendTitle= $  < Q^2 >=28.6$ GeV$^2$ \\$  < x>= 0.00093 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d28-x01-y01
Title=Charged Particle Spectra H1 $0.5 < \eta^* < 1.5 $ 
XLabel=$p_T^{max}$
YLabel=$1/N dn/dp_T^{max}$
LogY=1
YMax = 20
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0014 < x < 0.003 $
LegendTitle= $  < Q^2 > = 31.6$ GeV$^2$ \\$  < x >= 0.0021 $
END PLOT

BEGIN PLOT /H1_1996_I424463/d29-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 5 < Q^2 < 50$ GeV$^2$ \\$ 0.0001 < x < 0.001 $
LegendTitle= $  < Q^2 >=18.3$ GeV$^2$ \\$ < x>= 0.0014 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d30-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 5 < Q^2 < 10$ GeV$^2$ \\$ 0.0001 < x < 0.0002 $
LegendTitle= $ < Q^2 > =7$ GeV$^2$ \\$  < x >= 0.00016 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d31-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 6 < Q^2 < 10$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $ < Q^2 >=8.8$ GeV$^2$ \\$  < x >=0.00029 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d32-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $  < Q^2 >=13.1$ GeV$^2$ \\$  < x>= 0.00037 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d33-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0005 < x < 0.0008 $
LegendTitle= $  < Q^2 >=14$ GeV$^2$ \\$  < x >= 0.00064 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d34-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0008 < x < 0.0015 $
LegendTitle= $  < Q^2 >=14.3$ GeV$^2$ \\$  < x >=0.0011 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d35-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0015 < x < 0.004 $
LegendTitle= $  < Q^2 >=15.3$ GeV$^2$ \\$  < x >= 0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d36-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0005 < x < 0.0014 $
LegendTitle= $  < Q^2 >=28.6$ GeV$^2$ \\$  < x > 0.00093 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d37-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0014 < x < 0.003 $
LegendTitle= $ < Q^2 >=31.6$ GeV$^2$ \\$  < x> 0.00093 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d38-x01-y01
Title=Charged Particle Spectra H1 $p_T^* > 1 $ GeV
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 0.4
XMin = 0
XMax = 5
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.003 < x < 0.01 $
LegendTitle= $  < Q^2 >=34.7$ GeV$^2$ \\$  < x >= 0.0044 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d39-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 5 < Q^2 < 50$ GeV$^2$ \\$ 0.0001 < x < 0.001 $
LegendTitle= $ < Q^2> =18.3$ GeV$^2$ \\$  < x >= 0.000114 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d40-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 5 < Q^2 < 10$ GeV$^2$ \\$ 0.0001 < x < 0.0002 $
LegendTitle= $  < Q^2>=7.0$ GeV$^2$ \\$  < x>= 0.00016 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d41-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 6 < Q^2 < 10$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $  < Q^2>=8.8$ GeV$^2$ \\$  < x >= 0.00029 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d42-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0002 < x < 0.0005 $
LegendTitle= $ < Q^2>=13.1$ GeV$^2$ \\$  < x >=0.00037 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d43-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0005 < x < 0.0008 $
LegendTitle= $ < Q^2>=14.0$ GeV$^2$ \\$  < x >= 0.00064 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d44-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0008 < x < 0.0015 $
LegendTitle= $  < Q^2>=14.3$ GeV$^2$ \\$ < x >= 0.0011 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d45-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 10 < Q^2 < 20$ GeV$^2$ \\$ 0.0015 < x < 0.004 $
LegendTitle= $  < Q^2>=15.3$ GeV$^2$ \\$  < x >=0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d46-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0005 < x < 0.0014 $
LegendTitle= $ < Q^2 >=28.6$ GeV$^2$ \\$  < x > 0.00093 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d47-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.0014 < x < 0.003 $
LegendTitle= $  < Q^2 >=31.6$ GeV$^2$ \\$ < x >= 0.0021 $
END PLOT
BEGIN PLOT /H1_1996_I424463/d48-x01-y01
Title=Charged Particle Spectra H1 
XLabel=$\eta$
YLabel=$1/N dn/d\eta$
LogY=0
YMax = 3
XMin = 0
XMax = 5
#LegendTitle= $ 20 < Q^2 < 50$ GeV$^2$ \\$ 0.003 < x < 0.01 $
LegendTitle= $  < Q^2 >=34.7$ GeV$^2$ \\$  < x < 0.0044 $
END PLOT
