BEGIN PLOT /H1_2009_I810046/d01-x01-y01
Title=[Insert title for histogram d01-x01-y01 here]
XLabel=[Insert $x$-axis label for histogram d01-x01-y01 here]
#YLabel=[Insert $y$-axis label for histogram d01-x01-y01 here]
END PLOT



BEGIN PLOT /H1_2009_I810046/d02-x01-y01
Title=[Insert title for histogram d01-x01-y01 here]
XLabel=[Insert $x$-axis label for histogram d01-x01-y01 here]
#YLabel=[Insert $y$-axis label for histogram d01-x01-y01 here]
END PLOT



BEGIN PLOT /H1_2009_I810046/d03-x01-y01
Title=[Insert title for histogram d01-x01-y01 here]
XLabel=[Insert $x$-axis label for histogram d01-x01-y01 here]
#YLabel=[Insert $y$-axis label for histogram d01-x01-y01 here]
END PLOT



BEGIN PLOT /H1_2009_I810046/d04-x01-y01
Title=$K_{S}^0$ production cross-section
XLabel=$Q^2$
YLabel=$[pb/GeV^2]$
LogX=1
END PLOT


BEGIN PLOT /H1_2009_I810046/d05-x01-y01
Title=$K_{S}^0$ production cross-section
XLabel=$x$
YLabel=$[nb]$
LogX=1
END PLOT



BEGIN PLOT /H1_2009_I810046/d06-x01-y01
Title=$K_{S}^0$ production cross-section
XLabel=$p_{T}[GeV]$
YLabel=$[pb/GeV]$
END PLOT


BEGIN PLOT /H1_2009_I810046/d07-x01-y01
Title=$K_{S}^0$ production cross-section
XLabel=$\eta$
YLabel=$[pb]$
END PLOT



BEGIN PLOT /H1_2009_I810046/d08-x01-y01
Title=$\Lambda^0$ production cross-section
XLabel=$Q^2[GeV^2]$
YLabel=$[pb/GeV^2]$
LogX=1
END PLOT


BEGIN PLOT /H1_2009_I810046/d09-x01-y01
Title=$\Lambda^0$ production cross-section
XLabel=$x$
YLabel=$[nb]$
LogX=1
END PLOT



BEGIN PLOT /H1_2009_I810046/d10-x01-y01
Title=$\Lambda^0$ production cross-section
XLabel=$p_{T}[GeV]$
YLabel=$[pb/GeV]$
END PLOT



BEGIN PLOT /H1_2009_I810046/d11-x01-y01
Title=$\Lambda^0$ production cross-section
XLabel=$\eta$
YLabel=$[pb]$
END PLOT


# ... add more histograms as you need them ...
