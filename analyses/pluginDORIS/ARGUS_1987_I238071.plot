BEGIN PLOT /ARGUS_1987_I238071/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1987_I238071/d02-x01-y01
Title=$x$ in $\Upsilon(2S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}x$
XLabel=$x=(m_{\pi^+\pi^-}-2m_\pi)/(m_{\Upsilon(2S)}-m_{\Upsilon(1S)}-2m_\pi)$
LogY=0
END PLOT
