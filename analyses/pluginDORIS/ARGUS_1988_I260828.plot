BEGIN PLOT /ARGUS_1988_I260828/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT
