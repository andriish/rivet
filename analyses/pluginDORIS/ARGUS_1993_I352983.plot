BEGIN PLOT /ARGUS_1993_I352983/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1993_I352983/d01-x01-y02
Title=$K^0_S\pi^-$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1993_I352983/d01-x01-y03
Title=$K^0_S\pi^+$ mass distribution in $D^0\to K^0_S\pi^+\pi^-$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
