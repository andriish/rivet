BEGIN PLOT /ARGUS_1990_I296188/d01-x01-y01
Title=$B$ meson angular distribution
XLabel=$\cos\theta$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta$
LogY=0
END PLOT
