BEGIN PLOT /ARGUS_1991_I296187/
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1991_I296187/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^0\pi^+\pi^-$
END PLOT
BEGIN PLOT /ARGUS_1991_I296187/d03-x01-y01
Title=Cross section for $\gamma\gamma\to 2\pi^+2\pi^-$
END PLOT
BEGIN PLOT /ARGUS_1991_I296187/d05-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^0\rho^0$
END PLOT
