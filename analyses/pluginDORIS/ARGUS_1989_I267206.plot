BEGIN PLOT /ARGUS_1989_I267206/d01-x01-y01
Title=$D^{*+}$ helicity angle
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1989_I267206/d02-x01-y01
Title=$p_\ell$ for $\bar{B}^0\to D^{*+}\ell^-\bar\nu_\ell$
XLabel=$P_\ell$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1989_I267206/d03-x01-y01
Title=$q^2$ for $\bar{B}^0\to D^{*+}\ell^-\bar\nu_\ell$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
